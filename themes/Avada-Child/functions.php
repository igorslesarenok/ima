<?php
require_once ('kni_custom_functions.php');

function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
    wp_enqueue_style( 'kni_semantic_style', get_stylesheet_directory_uri() . '/css/dropdown.min.css');
    wp_enqueue_style( 'kni_uitransitions_style', get_stylesheet_directory_uri() . '/css/transition.min.css');
    wp_enqueue_style( 'kni_f_styles', get_stylesheet_directory_uri() . '/css/kni_f_styles.css');
	wp_enqueue_script ('kni_scripts', get_stylesheet_directory_uri() . '/js/kni_f_scripts.js', array( 'jquery' ), null, true);
	wp_enqueue_script ('kni_semantic_scripts', get_stylesheet_directory_uri() . '/js/dropdown.min.js', array( 'jquery' ), null, true);
	wp_enqueue_script ('kni_uitransitions_scripts', get_stylesheet_directory_uri() . '/js/transition.min.js', array( 'jquery' ), null, true);
	
	wp_localize_script('kni_scripts', 'kniKV', array(
			'nonce' => wp_create_nonce( 'kni-dkos0492ld' ),
			'url' => admin_url('admin-ajax.php')
		)
	);
	
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );


function admin_theme_enqueue_styles() {

    wp_enqueue_style( 'kni_admin_styles', get_stylesheet_directory_uri() . '/css/kni_admin_styles.css');

}

add_action( 'admin_enqueue_scripts', 'admin_theme_enqueue_styles' );

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );
