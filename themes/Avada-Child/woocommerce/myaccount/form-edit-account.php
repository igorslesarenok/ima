<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $wpdb, $member_levels_ids, $affiliate_levels_ids;

do_action( 'woocommerce_before_edit_account_form' ); ?>

<form class="woocommerce-EditAccountForm edit-account 111" action="" method="post" enctype="multipart/form-data" >

	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
	
	<div class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<strong><?php _e( 'Member ID:', 'woocommerce' ); ?> </strong>
		<span id="member_id" ><?php echo get_user_meta($user->data->ID,'ima_member_id',true); ?></span>
	</div>
	
	<div class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="account_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" value="<?php echo esc_attr( $user->billing_first_name ); ?>" />
	</div>
	<div class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
		<label for="account_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" value="<?php echo esc_attr( $user->billing_last_name ); ?>" />
	</div>
	<div class="clear"></div>

	<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>		
		<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" value="<?php echo esc_attr( $user->user_email ); ?>" />
	</div>
	
	<?php if( in_array(pmpro_getMembershipLevelsForUser()[0]->id, $affiliate_levels_ids) ){?>
	
		<div class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
			<label for="billing_company"><?php _e( 'Name', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_company" id="billing_company" value="<?php echo esc_attr( $user->billing_company ); ?>" />
		</div>
		
		<div class="clear"></div>
		
		<div id="account_logo_p" class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" >
			<label for="account_logo"><?php _e( 'Logo', 'woocommerce' ); ?> <span class="required">*</span></label>			
			
			<?php $attachment_url = get_user_meta($user->data->ID,'account_logo',true);?>
			
			<input type="hidden" name="account_logo" id="account_logo" value="<?php echo $attachment_url ?>" />
			
			<span class="account_logo_image" style="background-image: url(<?php if($attachment_url) {echo $attachment_url; }else { echo "/wp-content/themes/Avada-Child/images/dummy-f74b4666807c1d3d46086580423e5bc63e03aa9956a173a995025f1c7d8ca3ff.png"; } ?>)"></span>
			<?php echo do_shortcode('[ajax-file-upload unique_identifier="account_logo_mp_w"   on_success_set_input_value="#account_logo" allowed_extensions="png,PNG,jpg,JPG,jpeg,JPEG,bmp,BMP,gif,GIF" ]'); ?>		
			<?php if($attachment_url) { echo "<script type='text/javascript'>jQuery(function($){ $('#account_logo_p .afu-process-file .remove').removeAttr('disabled') }) </script>"; } ?>
		</div>

		<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="user_url"><?php _e( 'Website', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_url" id="user_url" value="<?php echo esc_attr( $user->user_url ); ?>" />
		</div>
		
		<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="user_physical_address"><?php _e( 'Physical address', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_physical_address" id="user_physical_address" value="<?php echo esc_attr( $user->user_physical_address ); ?>" onFocus="geolocate()" />
			
			<input type="hidden" name="user_street_number" id="street_number" value="<?php echo esc_attr( $user->user_street_number ); ?>" >
			<input type="hidden" name="user_street_name" id="route" value="<?php echo esc_attr( $user->user_street_name ); ?>" >
			<input type="hidden" name="user_locality" id="locality" value="<?php echo esc_attr( $user->billing_city ); ?>" >
			<input type="hidden" name="user_administrative_area_level_1" id="administrative_area_level_1" value="<?php echo esc_attr( $user->user_administrative_area_level_1 ); ?>" >
			<input type="hidden" name="user_country"  id="country" value="<?php echo esc_attr( $user->billing_country ); ?>" >
			<input type="hidden" name="user_postal_code" id="postal_code" value="<?php echo esc_attr( $user->billing_postcode ); ?>" >
			
			<script>			  
			  var placeSearch, autocomplete;
			  var componentForm = {
				street_number: 'short_name',
				route: 'long_name',
				locality: 'long_name',
				administrative_area_level_1: 'short_name',
				country: 'short_name',
				postal_code: 'short_name'
			  };

			  function initAutocomplete() {
				// Create the autocomplete object, restricting the search to geographical
				// location types.
				autocomplete = new google.maps.places.Autocomplete(
					/** @type {!HTMLInputElement} */(document.getElementById('user_physical_address')),
					{types: ['geocode']});

				// When the user selects an address from the dropdown, populate the address
				// fields in the form.
				autocomplete.addListener('place_changed', fillInAddress);
			  }
			  
			  
				  function fillInAddress() {
					// Get the place details from the autocomplete object.
					var place = autocomplete.getPlace();

					for (var component in componentForm) {
					  document.getElementById(component).value = '';
					  document.getElementById(component).disabled = false;
					}

					// Get each component of the address from the place details
					// and fill the corresponding field on the form.
					for (var i = 0; i < place.address_components.length; i++) {
					  var addressType = place.address_components[i].types[0];
					  if (componentForm[addressType]) {
						var val = place.address_components[i][componentForm[addressType]];
						document.getElementById(addressType).value = val;
					  }
					}
				
					
				  }
			  
			  // Bias the autocomplete object to the user's geographical location,
			  // as supplied by the browser's 'navigator.geolocation' object.
			  function geolocate() {
				if (navigator.geolocation) {
				  navigator.geolocation.getCurrentPosition(function(position) {
					var geolocation = {
					  lat: position.coords.latitude,
					  lng: position.coords.longitude
					};
					var circle = new google.maps.Circle({
					  center: geolocation,
					  radius: position.coords.accuracy
					});
					autocomplete.setBounds(circle.getBounds());
				  });
				}
			  }
			</script>
   
			 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmOM_ubaLlIRhXiARVXVpJhXI9BTKcycw&libraries=places&callback=initAutocomplete"
        async defer></script>
		
		</div>
		
		<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide affiliate_certificates_p">
			<?php $certificates_url = explode(",", get_user_meta($user->ID, 'affiliate_certificates', true) ); ?>
			<label for="affiliate_certificates"><?php _e( 'PDF certificates', 'woocommerce' ); ?> </label>
			<input type="hidden" class="woocommerce-Input woocommerce-Input--text input-text" name="affiliate_certificates" id="affiliate_certificates" value="<?php echo get_user_meta($user->ID, 'affiliate_certificates', true); ?>" />
			<div class="affiliate_certificates_box <?php if(get_user_meta($user->ID, 'affiliate_certificates', true)){echo "active";} ?>" >
				<?php 
					if(get_user_meta($user->ID, 'affiliate_certificates', true)){
						foreach($certificates_url as $url){			
							$file_name = preg_replace("/.*\//","",$url);
							$file_name = str_replace("'","",$file_name);
							echo "<a href=".$url." target='_blank' class='affiliate_certificates_a' >".$file_name."</a>";
						}
					}					
				?>
			</div>
			<?php if(get_user_meta($user->ID, 'affiliate_certificates', true)){echo "<script type='text/javascript'>jQuery(function($){ $('.affiliate_certificates_p .afu-process-file .remove').removeAttr('disabled') }) </script>";} ?>
			<?php echo do_shortcode('[ajax-file-upload unique_identifier="affiliate_certificates_mp_w" allowed_extensions="pdf" ]'); ?>		
		</div>
		
		<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" id="account_martial_arts_black_belt_wrapper">
			
			<label for="account_martial_arts_black_belt" class=""><?php _e( 'Martial arts in which the affiliate has a black belt', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="hidden" name="account_martial_arts_black_belt" value='<?php echo get_user_meta($user->ID, 'account_martial_arts_black_belt', true); ?>' >
			<select id="account_martial_arts_black_belt" class="ui fluid search selection dropdown" multiple="">
				<option value="">Select a method...</option>
				<option value="KRT">Karate</option>
				<option value="UFC">UFC</option>
				<option value="AKD">Aikido</option>
				<option value="KNGF">Kong fu</option>
				<option value="JUDO">Judo</option>
				<option value="ARNIS">Arnis</option>
				<option value="KRVMG">Krav Maga</option>
				<option value="KPP">Kapap</option>
				<option value="MMA">MMA</option>
				<option value="NJTS">Ninjutsu</option>
				<option value="K1">K1</option>
				<option value="BJJ">Brazilian jiu-jitsu (Bjj)</option>
				<option value="TKWND">Taekwondo</option>
				<option value="CPR">Capoeira</option>
				<option value="ESKR">Eskrima</option>
				<option value="JJ">Jujutsu</option>
				<option value="PNKR">Pankration</option>
				<option value="AJ">Aiki Jutsu</option>
				<option value="KD">Kendo</option>
				<option value="SH">Shi Heun</option>
				<option value="KJ">Kempo Jitsu</option>
			</select>
			
			<?php $martial_arts_methods = json_decode(get_user_meta($user->ID, 'account_martial_arts_black_belt', true), true);?>
			
			<script type="text/javascript">
				jQuery(function($){
					
					$('#account_martial_arts_black_belt').dropdown('set selected',[<?php 						
						if(count($martial_arts_methods)){
							foreach ( $martial_arts_methods as $k => $value ){
								echo "'".$value."',";
							}
						}
					?>]);
					
					$('#account_martial_arts_black_belt').dropdown('setting', 'onChange',function(value, text, $choice) {					
										
						var jsonMABB = {};
						
						if($('.martial_arts_black_belt_number_wrapper').length && value.length < 1 ){
							$('.martial_arts_black_belt_number_wrapper').remove();
						}
						
						var empty = true;
						value.forEach(function(item, i, value) {
							jsonMABB[item] = $("#account_martial_arts_black_belt option[value="+item+"]").text();
							
							if($("#account_martial_arts_black_belt option[value="+item+"]").text()){
								empty = false;
							}
							
						});
					
						if( !empty ){
							$('input[name=account_martial_arts_black_belt]').val(JSON.stringify(jsonMABB));
						} else {
							$('input[name=account_martial_arts_black_belt]').val('');
						}
						
						
					})
					
					$('#account_martial_arts_black_belt').dropdown('setting', 'onAdd',function(addedValue, addedText, $addedChoice) {
						
						if(!$('.martial_arts_black_belt_number_wrapper').length){
							$('#account_martial_arts_black_belt_wrapper').after('<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide martial_arts_black_belt_number_wrapper"><label>Black belt number per each of the above martial arts</label><div class="martial_arts_belt_number_wrapper" ><table width="100%" cellpadding="0" cellspacing="0" border="0"><thead><tr><th>Martial arts</th><th>Black belt number</th></tr></thead><tbody><tr class="tr_'+addedValue+'" ><td>'+addedText+'</td><td><input type="text" class="belt_number" name="'+addedValue+'_black_belt_number"  /></td></tr></tbody></table></div></div>');
						} else{
							$('.martial_arts_black_belt_number_wrapper tbody').append('<tr class="tr_'+addedValue+'" ><td>'+addedText+'</td><td><input type="text" class="belt_number" name="'+addedValue+'_black_belt_number"  /></td></tr>')
						}						
						
					})
					
					$('#account_martial_arts_black_belt').dropdown('setting', 'onRemove',function(removedValue, removedText, $removedChoice) {
						$('.martial_arts_black_belt_number_wrapper .tr_'+removedValue).remove();
					})
					
				})
			</script>	  
			
		</div>
		
		<?php 			
			if(count($martial_arts_methods)){
		?>
			<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide martial_arts_black_belt_number_wrapper"><label>Black belt number per each of the above martial arts <span class="required">*</span></label>		
				<div class="martial_arts_belt_number_wrapper" >
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<thead>
							<tr>
								<th>Martial arts</th>
								<th>Black belt number</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($martial_arts_methods as $k => $value){?>
							<tr class="tr_<?php echo $k; ?>" >
								<td><?php echo $value; ?></td>
								<td><input type="text" class="belt_number" name="<?php echo $k; ?>_black_belt_number" value="<?php echo get_user_meta( $user->ID, $k.'_black_belt_number', true); ?>" /></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>				
				</div>			
			</div>
		
		<?php 
			}
		?>
	
	<?php }else if( in_array(pmpro_getMembershipLevelsForUser()[0]->id,$member_levels_ids) ) { ?>	
		
		
		<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="billing_phone"><?php _e( 'Phone Number', 'woocommerce' ); ?> <span class="required">*</span></label>		
			<input type="text" class="woocommerce-Input woocommerce-Input--email input-text" name="billing_phone" id="billing_phone" value="<?php echo esc_attr( $user->billing_phone ); ?>" />
		</div>
		
		<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" id="account_martial_arts_method_wrapper">
			
			<label for="account_martial_arts_method" class=""><?php _e( 'Martial Arts method', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="hidden" name="account_martial_arts_method" value='<?php echo get_user_meta($user->ID, 'account_martial_arts_method', true); ?>' >
			<select id="account_martial_arts_method" class="ui fluid search selection dropdown" multiple="">
				<option value="">Select a method...</option>
				<option value="KRT">Karate</option>
				<option value="UFC">UFC</option>
				<option value="AKD">Aikido</option>
				<option value="KNGF">Kong fu</option>
				<option value="JUDO">Judo</option>
				<option value="ARNIS">Arnis</option>
				<option value="KRVMG">Krav Maga</option>
				<option value="KPP">Kapap</option>
				<option value="MMA">MMA</option>
				<option value="NJTS">Ninjutsu</option>
				<option value="K1">K1</option>
				<option value="BJJ">Brazilian jiu-jitsu (Bjj)</option>
				<option value="TKWND">Taekwondo</option>
				<option value="CPR">Capoeira</option>
				<option value="ESKR">Eskrima</option>
				<option value="JJ">Jujutsu</option>
				<option value="PNKR">Pankration</option>
				<option value="AJ">Aiki Jutsu</option>
				<option value="KD">Kendo</option>
				<option value="SH">Shi Heun</option>
				<option value="KJ">Kempo Jitsu</option>
				
			</select>
			
			<?php $martial_arts_methods = json_decode(get_user_meta($user->ID, 'account_martial_arts_method', true), true); ?>
			
			<script type="text/javascript">
				jQuery(function($){
					$('#account_martial_arts_method').dropdown('set selected',[<?php 						
						if(count($martial_arts_methods)){
							foreach ( $martial_arts_methods as $k => $value ){
								echo "'".$value."',";
							}
						}
					?>]);
										
					
					$('#account_martial_arts_method').dropdown('setting', 'onChange',function(value, text, $choice) {						
											
						var jsonMA = {};
												
						var empty = true;
						value.forEach(function(item, i, value) {
							jsonMA[item] = $("#account_martial_arts_method option[value="+item+"]").text();
							
							if($("#account_martial_arts_method option[value="+item+"]").text()){
								empty = false;
							}
							
						});
					
						if( !empty ){
							$('input[name=account_martial_arts_method]').val(JSON.stringify(jsonMA));
						} else {
							$('input[name=account_martial_arts_method]').val('');
						}						
						
					})
					
				})
			</script>	  
		</div>	
	<?php } ?>
	
	<div class="clear"></div>

	<?php do_action( 'woocommerce_edit_account_form' ); ?>

	<div>
		<?php wp_nonce_field( 'save_account_details' ); ?>
		<input type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>" />
		<input type="hidden" name="action" value="save_account_details" />
	</div>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
