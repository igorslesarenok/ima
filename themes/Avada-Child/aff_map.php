<?php
/**
 * Template name: Centers Map
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>
<section id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
	<?php while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
			<?php 
			
				global $wpdb;
				
				$order_by = $_GET["order_by"];
				$order = $_GET["order"];
				$aff_search = $_GET["aff_search"];
				
				$affiliates = $wpdb->get_results("SELECT (u.id) as user_id, (a.name) as aff_name, u.user_url FROM $wpdb->pmpro_affiliates a LEFT JOIN $wpdb->users u ON a.affiliateuser = u.user_login WHERE a.enabled = 1 ");
				
				$totalrows = count($affiliates);
				
				if(count($affiliates)){
					foreach($affiliates as $k => $affiliate){
						$affiliate ->logo = get_user_meta($affiliate->user_id, "account_logo" ,true);
						$affiliate ->billing_address_1 = get_user_meta($affiliate->user_id, "billing_address_1" ,true);
						$affiliate ->billing_state = get_user_meta($affiliate->user_id, "billing_state" ,true);
						$affiliate ->billing_city = get_user_meta($affiliate->user_id, "billing_city" ,true);
						$affiliate ->billing_country = WC()->countries->countries[get_user_meta($affiliate->user_id, "billing_country" ,true)];
						$affiliate ->billing_postcode = get_user_meta($affiliate->user_id, "billing_postcode" ,true);
					}
				}
				
			?>
		
			<div id="aff_map"></div>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmOM_ubaLlIRhXiARVXVpJhXI9BTKcycw"></script>
			
			<script>
				jQuery(function($) {	

					var affiliates = <?php echo json_encode($affiliates); ?>;
					var map, infowindow, aff, valBox;
					
					console.log(affiliates);
					
					//Load map
					function initMap() {
						
						map = new google.maps.Map(document.getElementById('aff_map'), {
							zoom: 7,
							center: {lat: 31.5, lng: 34.75},
							scrollwheel: false
						});
						
						infowindow = new google.maps.InfoWindow({
							content: '345'
						});
						
						google.maps.event.addListenerOnce(map, 'tilesloaded', addAffiliates);//ждем полную загрузку карты
						
					};// end of  initMap()

					google.maps.event.addDomListener(window, 'load', initMap);
					
					function addAffiliates() {
						
						if( affiliates.length ){
							$.each(affiliates, function(ind,e){
								geocodeAddress(e, infowindow, map);	
							})
						}
						
						
					}
					
					
					function geocodeAddress(e, infowindow, map) {
					  var geocoder = new google.maps.Geocoder();
					  
					  var address = new Array();
						
						if(e.billing_address_1 != '' ){
							address.push(e.billing_address_1)
						}
						
						if(e.billing_city != '' ){
							address.push(e.billing_city)

						}
						
						if(e.billing_state != '' ){
							address.push(e.billing_state)

						}
						
						if(e.billing_country != '' ){
							address.push(e.billing_country)
						}
						
						if(e.billing_postcode != '' ){
							address.push(e.billing_postcode)
						}
						
					  address = address.join(', ');
					  
					  geocoder.geocode({						  
						address: address,
					  }, function(result, status) {
						if (status == 'OK' && result.length > 0) {
							
							console.log(result)
							
							image = {
										  url: e.logo,
										  size: new google.maps.Size(60, 60),
										  origin: new google.maps.Point(0, 0),
										  anchor: new google.maps.Point(15,15), 
										  scaledSize: new google.maps.Size(30, 30)
										};
							
						  var marker = new google.maps.Marker({
							position: result[0].geometry.location,
							map: map,
							icon: image,
							size: new google.maps.Size(71, 71)
						  });
							
						  google.maps.event.addListener(marker, 'click', function() {
							var content = ('<div style="text-align: center;" ><h1>'+e.aff_name+'</h1><div style="width: 100px; height: 100px; margin: 0 auto 10px auto; background-image: url('+e.logo+') " ></div><div class="aff_url" ><strong>Website: </strong><a href="'+e.user_url+'" target="_blank" >'+e.user_url+'</a></div><div class="aff_address" ><strong>Physical address: </strong>'+address+'</div> </div> ');

							infowindow.setContent(content);
							infowindow.open(map, this);
						  });
						} else {
						  //alert("geocoder returns status:" + status)
						}
					  });
					}
					
				});
			</script>

			<div id="aff_list">
			
				<div class="controls">
					<div class="sort" ><label for="aff_sort_by">Order by:  </label>
						<select id="aff_sort_by" >
							<option value="name" >Name</option>							
							<option value="address" >Physical address</option>							
						</select>	
						
						<a class="direction active" href="?aff_order=asc"  >&#8595;</a> <a class="direction" href="?aff_order=desc" >&#8593;</a>
						
					</div>
					<div class="search">
						<label for="aff_search">Search: </label><input type="text" id="aff_search" name="aff_search" />
					</div>
				</div>
			
				<div class="kni_row heads">
					<div class="kni_td" ><?php _e('Name', 'pmpro_affiliates'); ?></div>
					<div class="kni_td" ><?php _e('Logo', 'pmpro_affiliates'); ?></div>
					<div class="kni_td" ><?php _e('Website', 'pmpro_affiliates'); ?></div>
					<div class="kni_td" ><?php _e('Physical address', 'pmpro_affiliates'); ?></div>
				</div>
				
				<?php
					
					//pagination
					$limit = 10;
					
					if(isset($_GET['pn'])){
						$pn = $_GET['pn'];
					} else {
						$pn = 1;
					}
					
					$end = $pn * $limit;
					$start = $end - $limit;
					
					$affiliates = $wpdb->get_results("SELECT (u.id) as user_id, (a.name) as aff_name, u.user_url FROM $wpdb->pmpro_affiliates a LEFT JOIN $wpdb->users u ON a.affiliateuser = u.user_login WHERE a.enabled = 1 LIMIT $start, $limit ");
				
					if(count($affiliates)){					
					
						foreach($affiliates as $k => $affiliate){
							$affiliate ->logo = get_user_meta($affiliate->user_id, "account_logo" ,true);
							$affiliate ->billing_address_1 = get_user_meta($affiliate->user_id, "billing_address_1" ,true);
							$affiliate ->billing_state = get_user_meta($affiliate->user_id, "billing_state" ,true);
							$affiliate ->billing_city = get_user_meta($affiliate->user_id, "billing_city" ,true);
							$affiliate ->billing_country = WC()->countries->countries[get_user_meta($affiliate->user_id, "billing_country" ,true)];
							$affiliate ->billing_postcode = get_user_meta($affiliate->user_id, "billing_postcode" ,true);
						}
				
				?>
				
					<?php foreach($affiliates as $k => $affiliate) { ?>
						<div class="kni_row">
							<div class="kni_td" ><?php echo $affiliate->aff_name; ?></div>
							<div class="kni_td" ><div class="aff_logo" style="background-image: url(<?php echo $affiliate->logo; ?>)" ></div></div>
							<div class="kni_td" ><a href="<?php echo $affiliate->user_url; ?>" target="_blank" ><?php echo $affiliate->user_url; ?></a></div>
							<div class="kni_td" >
								<?php 
								 
								$address = array(); 
								
								if($affiliate->billing_address_1 != '' && preg_match("/(.+?), (.+?)/i", $affiliate->billing_address_1) == 1 ){
									$address[] = $affiliate->billing_address_1;
								}
								
								if($affiliate->billing_city != '' ){
									$address[] = $affiliate->billing_city;
								}
								
								if($affiliate->billing_state != '' ){
									$address[] = $affiliate->billing_state;
								}
								
								if($affiliate->billing_country != '' ){
									$address[] = $affiliate->billing_country;
								}
								
								if($affiliate->billing_postcode != '' ){
									$address[] = $affiliate->billing_postcode;
								}
								
								echo implode(", ",$address); 
								
								?>
							</div>
						</div>
					<?php } ?>
					
				<?php } ?>
			
				
			</div>
			
				<?php
					echo pmpro_getPaginationString($pn, $totalrows, $limit, 1, "?");
				?>
				
		</div>	
		
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
</section>
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
