/**
 * WooCommerce MyAccount page
 */ 
/*-Account details*/
/*--Logo*/
jQuery(function($){
	window.addEventListener( "afu_file_uploaded", function(e){
		
		if(e.data.response.settings.unique_identifier == "account_logo_mp_w"){

			if( "undefined" !== typeof e.data.response.media_uri ) {
				console.log( e.data.response.media_uri ); // the uploaded media URL
				$(".account_logo_image").css("background-image","url("+e.data.response.media_uri+")")
			}
		}
		
		
		if(e.data.response.settings.unique_identifier == "affiliate_certificates_mp_w"){
			
			var inputValH = $('#affiliate_certificates').val();

			$('.affiliate_certificates_p .affiliate_certificates_box').addClass('active').append('<a href="'+e.data.response.media_uri+'" target="_blank" class="affiliate_certificates_a" >'+e.data.response.file.name+'</a> ');
			
			if(inputValH.length > 0){
				inputValH = inputValH+",";
			}
			
			$('#affiliate_certificates').val(inputValH+"'"+e.data.response.media_uri+"'");
	
		}
		
	}, false);
	
	window.addEventListener( "afu_file_removed", function(e){
		
		if(e.data.container["0"].firstElementChild.defaultValue == "account_logo_mp_w"){
			$(".account_logo_image").css("background-image","url(/wp-content/themes/Avada-Child/images/dummy-f74b4666807c1d3d46086580423e5bc63e03aa9956a173a995025f1c7d8ca3ff.png)")
		}
		
		if(e.data.container["0"].firstElementChild.defaultValue == "affiliate_certificates_mp_w"){

			$('.affiliate_certificates_a').remove();
			$('.affiliate_certificates_p .affiliate_certificates_box').removeClass('active');
			$('#affiliate_certificates').val('')
		}
		
	}, false);
	
})

/**
 * Another functions
 */

jQuery(function($){
	var hash = null;
	hash = decodeURIComponent(getParameterByName("elhash"));
	
	if( hash != 'null' && hash.length ){
		window.location.hash  = hash;
	}
	
})

//get query value from url
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
