<?php

/**
 * Subscription & Membership
 */

//ID's of membershps
global $member_levels_ids, $affiliate_levels_ids;
$member_levels_ids = array(1,2,6,7);
$affiliate_levels_ids = array(3,5);
 
/* -Personal area: */
/* --Add new tabs to WooCommerce 'My Account' page */
function add_my_memberships_endpoint() {
    add_rewrite_endpoint( 'my-memberships-endpoint', EP_ROOT | EP_PAGES );
    add_rewrite_endpoint( 'my-tests', EP_ROOT | EP_PAGES );
	flush_rewrite_rules();
	
	
	
} 
add_action( 'init', 'add_my_memberships_endpoint' );

function my_memberships_query_vars( $vars ) {
	$vars[] = 'my-memberships-endpoint';
	$vars[] = 'my-tests';
	$vars[] = 'my-tests';
	return $vars;
}
add_filter( 'query_vars', 'my_memberships_query_vars', 0 );

function my_custom_insert_after_helper( $items, $new_items, $after ) {
	// Search for the item position and +1 since is after the selected item key.
	$position = array_search( $after, array_keys( $items ) ) + 1;

	// Insert the new item.
	$array = array_slice( $items, 0, $position, true );
	$array += $new_items;
	$array += array_slice( $items, $position, count( $items ) - $position, true );

    return $array;
}

function my_memberships_menu_items( $items ) {
	$new_items = array();
	$new_items['my-memberships-endpoint'] = __( 'My Memberships', 'woocommerce' );
	$new_items['edit-account'] = __( 'Account details', 'woocommerce' );
	$new_items['my-tests'] = __( 'My tests', 'woocommerce' );

	// Add the new item after `orders`.
	return my_custom_insert_after_helper( $items, $new_items, 'dashboard' );
	
}
add_filter( 'woocommerce_account_menu_items', 'my_memberships_menu_items' );


/*
 My memberships tab
*/
function my_memberships_endpoint_content() {  
	global $wpdb, $pmpro_msg, $pmpro_msgt, $pmpro_levels, $current_user, $levels, $pmpro_affiliates_settings, $member_levels_ids, $affiliate_levels_ids;
	
	$aff_discount_member_reg = $pmpro_affiliates_settings['aff_discount_member_reg'];
	$aff_commission_member_reg = $pmpro_affiliates_settings['aff_commission_member_reg'];
	$aff_discount_affiliate_reg = $pmpro_affiliates_settings['aff_discount_affiliate_reg'];
	$aff_commission_affiliate_reg = $pmpro_affiliates_settings['aff_commission_affiliate_reg'];
	
	// $atts    ::= array of attributes
	// $content ::= text within enclosing form of shortcode element
	// $code    ::= the shortcode found, when == callback name
	// examples: [pmpro_account] [pmpro_account sections="membership,profile"/]

	//Extract the user-defined sections for the shortcode
	$sections =  array('membership','invoices', 'promo_code', 'linked_members');	
	ob_start();
	
	//if a member is logged in, show them some info here (1. past invoices. 2. billing information with button to update.)
	if(pmpro_hasMembershipLevel())
	{
		$ssorder = new MemberOrder();
		$ssorder->getLastMemberOrder();
		$mylevels = pmpro_getMembershipLevelsForUser();
		$pmpro_levels = pmpro_getAllLevels(false, true); // just to be sure - include only the ones that allow signups
		$invoices = $wpdb->get_results("SELECT *, UNIX_TIMESTAMP(timestamp) as timestamp FROM $wpdb->pmpro_membership_orders WHERE user_id = '$current_user->ID' AND status NOT IN('refunded', 'review', 'token', 'error') ORDER BY timestamp DESC LIMIT 6");
		
		
		?>	
		
	<div id="pmpro_account">		
		<?php if(in_array('membership', $sections) || in_array('memberships', $sections)) { ?>
			<div id="pmpro_account-membership" class="pmpro_box">
				
				<h3><?php _e("My Memberships", 'paid-memberships-pro' );?></h3>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<thead>
						<tr>
							<th><?php _e("Level", 'paid-memberships-pro' );?></th>
							<th><?php _e("Billing", 'paid-memberships-pro' ); ?></th>
							<th><?php _e("Expiration", 'paid-memberships-pro' ); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach($mylevels as $level) {
						?>
						<tr>
							<td class="pmpro_account-membership-levelname">
								<?php echo $level->name?>
								<div class="pmpro_actionlinks">
									<?php do_action("pmpro_member_action_links_before"); ?>
									
									<?php if( array_key_exists($level->id, $pmpro_levels) && pmpro_isLevelExpiringSoon( $level ) ) { ?>
										<a href="<?php echo pmpro_url("checkout", "?level=" . $level->id, "https")?>"><?php _e("Renew", 'paid-memberships-pro' );?></a>
									<?php } ?>

									<?php if((isset($ssorder->status) && $ssorder->status == "success") && (isset($ssorder->gateway) && in_array($ssorder->gateway, array("authorizenet", "paypal", "stripe", "braintree", "payflow", "cybersource"))) && pmpro_isLevelRecurring($level)) { ?>
										<a href="<?php echo pmpro_url("billing", "", "https")?>"><?php _e("Update Billing Info", 'paid-memberships-pro' ); ?></a>
									<?php } ?>
									
									<?php 
										//To do: Only show CHANGE link if this level is in a group that has upgrade/downgrade rules
										if(count($pmpro_levels) > 1 && !defined("PMPRO_DEFAULT_LEVEL")) { ?>
										<a href="<?php echo pmpro_url("levels")?>" id="pmpro_account-change"><?php _e("Change", 'paid-memberships-pro' );?></a>
									<?php } ?>
									<a href="<?php echo pmpro_url("cancel", "?levelstocancel=" . $level->id)?>" id="pmpro_account-cancel"><?php _e("Cancel", 'paid-memberships-pro' );?></a>
									<?php do_action("pmpro_member_action_links_after"); ?>
								</div> <!-- end pmpro_actionlinks -->
							</td>
							<td class="pmpro_account-membership-levelfee">
								<p><?php echo pmpro_getLevelCost($level, true, true);?></p>
							</td>
							<td class="pmpro_account-membership-expiration">
							<?php 
								if($level->enddate)
									$expiration_text = date(get_option('date_format'), $level->enddate);
								else
									$expiration_text = "---";

							    	echo apply_filters( 'pmpro_account_membership_expiration_text', $expiration_text, $level );
							?>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<?php //Todo: If there are multiple levels defined that aren't all in the same group defined as upgrades/downgrades ?>
				<div class="pmpro_actionlinks">
					<a href="<?php echo pmpro_url("levels")?>"><?php _e("View all Membership Options", 'paid-memberships-pro' );?></a>
				</div>

			</div> <!-- end pmpro_account-membership -->
		<?php } ?>
		
		<?php if(in_array('profile', $sections)) { ?>
			<div id="pmpro_account-profile" class="pmpro_box">	
				<?php wp_get_current_user(); ?> 
				<h3><?php _e("My Account", 'paid-memberships-pro' );?></h3>
				<?php if($current_user->user_firstname) { ?>
					<p><?php echo $current_user->user_firstname?> <?php echo $current_user->user_lastname?></p>
				<?php } ?>
				<ul>
					<?php do_action('pmpro_account_bullets_top');?>
					<li><strong><?php _e("Username", 'paid-memberships-pro' );?>:</strong> <?php echo $current_user->user_login?></li>
					<li><strong><?php _e("Email", 'paid-memberships-pro' );?>:</strong> <?php echo $current_user->user_email?></li>
					<?php do_action('pmpro_account_bullets_bottom');?>
				</ul>
				<div class="pmpro_actionlinks">
					<a href="<?php echo admin_url('profile.php')?>" id="pmpro_account-edit-profile"><?php _e("Edit Profile", 'paid-memberships-pro' );?></a>
					<a href="<?php echo admin_url('profile.php')?>" id="pmpro_account-change-password"><?php _e('Change Password', 'paid-memberships-pro' );?></a>
				</div>
			</div> <!-- end pmpro_account-profile -->
		<?php } ?>
	
		<?php if(in_array('invoices', $sections) && !empty($invoices)) { ?>		
		<div id="pmpro_account-invoices" class="pmpro_box">
			<h3><?php _e("Past Invoices", 'paid-memberships-pro' );?></h3>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<th><?php _e("Date", 'paid-memberships-pro' ); ?></th>
						<th><?php _e("Level", 'paid-memberships-pro' ); ?></th>
						<th><?php _e("Amount", 'paid-memberships-pro' ); ?></th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$count = 0;
					foreach($invoices as $invoice) 
					{ 
						if($count++ > 4)
							break;

						//get an member order object
						$invoice_id = $invoice->id;
						$invoice = new MemberOrder;
						$invoice->getMemberOrderByID($invoice_id);
						$invoice->getMembershipLevel();						
						?>
						<tr id="pmpro_account-invoice-<?php echo $invoice->code; ?>">
							<td><a href="<?php echo pmpro_url("invoice", "?invoice=" . $invoice->code)?>"><?php echo date_i18n(get_option("date_format"), $invoice->timestamp)?></td>
							<td><?php if(!empty($invoice->membership_level)) echo $invoice->membership_level->name; else echo __("N/A", 'paid-memberships-pro' );?></td>
							<td><?php echo pmpro_formatPrice($invoice->total)?></td>
						</tr>
						<?php 
					}
				?>
				</tbody>
			</table>						
			<?php if($count == 6) { ?>
				<div class="pmpro_actionlinks"><a href="<?php echo pmpro_url("invoice"); ?>"><?php _e("View All Invoices", 'paid-memberships-pro' );?></a></div>
			<?php } ?>
		</div> <!-- end pmpro_account-invoices -->
		<?php } ?>
		
		<?php if(in_array('links', $sections) && (has_filter('pmpro_member_links_top') || has_filter('pmpro_member_links_bottom'))) { ?>
		<div id="pmpro_account-links" class="pmpro_box">
			<h3><?php _e("Member Links", 'paid-memberships-pro' );?></h3>
			<ul>
				<?php 
					do_action("pmpro_member_links_top");
				?>
				
				<?php 
					do_action("pmpro_member_links_bottom");
				?>
			</ul>
		</div> <!-- end pmpro_account-links -->		
		<?php } ?>
		
		<?php
				
			$affiliate = $wpdb->get_row("SELECT id, code, enabled, UNIX_TIMESTAMP(last_payout_date) as last_payout_date, last_payout_money FROM $wpdb->pmpro_affiliates WHERE affiliateuser = '" . esc_sql($current_user->user_login) . "' LIMIT 1");

			$affiliate_id = $affiliate->id;
			
			$linked_members = $wpdb->get_results("SELECT user_id FROM $wpdb->pmpro_membership_orders WHERE affiliate_id = '".$affiliate_id."' ");
			
		?>
		
		
		<?php if( in_array('promo_code', $sections) && in_array(pmpro_getMembershipLevelsForUser()[0]->id, $affiliate_levels_ids)  ) { ?>		
		<div id="kni_promo_code" class="pmpro_box">
			<h3><?php _e("Promo code", 'paid-memberships-pro' );?></h3>
			<table width="100%" cellpadding="1" cellspacing="0" border="1">
				<thead>
					<tr>
						<th><?php _e("Code", 'paid-memberships-pro' ); ?></th>
						<th><?php _e("Enabled", 'paid-memberships-pro' ); ?></th>
						<th><?php _e("Earnings ( for all time )", 'paid-memberships-pro' ); ?></th>
						<th><?php _e("Earnings ( from last payout )", 'paid-memberships-pro' ); ?></th>
						<th><?php _e("Last payout", 'paid-memberships-pro' ); ?></th>
					</tr>
				</thead>
				<tbody>
				
					<tr id="pmpro_account-promo_code-<?php echo $affiliate_id; ?>">
						<td><?php echo $affiliate->code; ?></td>
						<td>
							<?php if( $affiliate->enabled ) { $enabled =  "Yes"; } else { $enabled =  "No"; } ?>
							<span class="<?php echo $enabled; ?>" ><?php echo $enabled; ?></span>
						</td>
						<td>
							<?php 
															
								$users = $wpdb->get_results("SELECT membership_id  FROM $wpdb->pmpro_membership_orders WHERE affiliate_id = '" . esc_sql($affiliate->id) . "' AND status NOT IN('pending', 'error', 'refunded', 'refund', 'token', 'review')");
								
								$earnings = 0;
								
								if( count($users) ){
									foreach( $users as $k => $user ){
										$levelDetails = $wpdb->get_row("SELECT * FROM $wpdb->pmpro_membership_levels WHERE id = '" . esc_sql( $user->membership_id) . "' ");
										
										if(count($levelDetails)){
											if( $levelDetails->cycle_period == "Month" && $levelDetails->cycle_number > 0 ){
												if( in_array($user->membership_id, $aff_commission_member_reg['levels']) ){
													$earnings = $earnings + ($levelDetails->cycle_number*$aff_commission_member_reg['value']);
												}else if( in_array($user->membership_id, $aff_commission_affiliate_reg['levels']) ){
													$earnings = $earnings + ($levelDetails->cycle_number*$aff_commission_affiliate_reg['value']);
												}
												
											}
										}
										
									}
								}							
								
								echo pmpro_formatPrice($earnings);
									
							?>
						</td>							
						<td>
						
							<?php
								
								
								$last_payout_date = $wpdb->get_var("SELECT UNIX_TIMESTAMP(last_payout_date) as last_payout_date FROM $wpdb->pmpro_affiliates WHERE id = '" . esc_sql($affiliate->id) . "' ");
																
								$users = $wpdb->get_results("SELECT membership_id  FROM $wpdb->pmpro_membership_orders WHERE affiliate_id = '" . esc_sql($affiliate->id) . "' AND UNIX_TIMESTAMP(timestamp) > ".$last_payout_date."  AND status NOT IN('pending', 'error', 'refunded', 'refund', 'token', 'review')");
								
								$earnings = 0;
								
								if( count($users) ){
									foreach( $users as $k => $user ){
										$levelDetails = $wpdb->get_row("SELECT * FROM $wpdb->pmpro_membership_levels WHERE id = '" . esc_sql( $user->membership_id) . "' ");
										
										if(count($levelDetails)){
											if( $levelDetails->cycle_period == "Month" && $levelDetails->cycle_number > 0 ){
												if( in_array($user->membership_id, $aff_commission_member_reg['levels']) ){
													$earnings = $earnings + ($levelDetails->cycle_number*$aff_commission_member_reg['value']);
												}else if( in_array($user->membership_id, $aff_commission_affiliate_reg['levels']) ){
													$earnings = $earnings + ($levelDetails->cycle_number*$aff_commission_affiliate_reg['value']);
												}
												
											}
										}
										
									}
								}						
								
								
								echo pmpro_formatPrice($earnings);
							?>
						
						</td>							
						<td><?php echo date_i18n("F j, Y g:i a", $affiliate->last_payout_date)." - <strong>".pmpro_formatPrice($affiliate->last_payout_money )."</strong>"; ?></td>							
						
					</tr>
						
				</tbody>
			</table>						
			
		</div> <!-- end pmpro_account-linked_members -->
		<?php } ?>
		
		<?php 
			
			$limit = 8; 		
			
			 //search query
			if(isset($_REQUEST['q']))
				$q = sanitize_text_field($_REQUEST['q']);
			else
				$q = "";
			
			//page number
			if(isset($_REQUEST['pn']))
				$pn = sanitize_text_field(trim($_REQUEST['pn']));
			else
				$pn = 1;
			
			$end = $pn * $limit;
			$start = $end - $limit;
			
			$linked_members_all = $wpdb->get_results("SELECT (b.ID) as user_id FROM $wpdb->pmpro_membership_orders a LEFT JOIN $wpdb->users b ON a.user_id = b.ID WHERE affiliate_id = '".$affiliate_id."' AND a.user_id = b.ID ");			
		
			$totalmembers = count($linked_members);
			
			//for pagination
			$linked_members = $wpdb->get_results("SELECT (b.ID) as user_id FROM $wpdb->pmpro_membership_orders a LEFT JOIN $wpdb->users b ON a.user_id = b.ID WHERE affiliate_id = '".$affiliate_id."' AND a.user_id = b.ID LIMIT $start, $limit ");
			
			
			//Searching
			if( $q != "" ){
				if( count($linked_members_all) ){
					
					$l_mmbrs = $linked_members_all;
					$linked_members = array();
					
					foreach( $l_mmbrs as $k => $v ){
						
						$ima_member_id = get_user_meta($v->user_id, "ima_member_id", true);
						
						if( $q == $ima_member_id ){
														
							$linked_members[0] = $v;
							break;
						}
										
						
					}
				}
			}
					
		?>
		
		<?php if( in_array('linked_members', $sections) && in_array(pmpro_getMembershipLevelsForUser()[0]->id, $affiliate_levels_ids) ) { ?>		
		<div id="kni_linked_members" class="pmpro_box">
			<h3><?php _e("View linked members", 'paid-memberships-pro' );?></h3>
			<form class="member_search_box" action="/my-account/my-memberships-endpoint/#kni_linked_members" method="GET" >
				<div class="search_form_field search_input" > 
					<label for="members_search" >Search: </label>
					<input type="text" name="q" id="members_search" placeholder="Enter Member ID..." value="<?php echo $q ?>" /> 
					
				</div>
				<div class="search_form_field submit_btn" >
					<button type="submit"></button>
				</div>
			</form>
			<table width="100%" cellpadding="1" cellspacing="0" border="1">
				<thead>
					<tr>
						<th><?php _e("Full Name", 'paid-memberships-pro' ); ?></th>
						<th><?php _e("Email", 'paid-memberships-pro' ); ?></th>
						<th><?php _e("Phone Number", 'paid-memberships-pro' ); ?></th>
						<th><?php _e("Member ID", 'paid-memberships-pro' ); ?></th>
						<th><?php _e("Martial Art Certificates granted by affiliate", 'paid-memberships-pro' ); ?></th>
					</tr>
				</thead>
				<tbody>
				<?php 
		
					if( count($linked_members) ){
						
						foreach( $linked_members as $k => $member ){
							
							
							//$user_ = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $wpdb->users WHERE ID = '$member->user_id' "));
							
							//if($user_){
																
								$first_name = get_user_meta($member->user_id, "first_name", true);
								$email = get_user_meta($member->user_id, "billing_email", true);
								$phone = get_user_meta($member->user_id, "billing_phone", true);
								$member_ID = get_user_meta($member->user_id, "ima_member_id", true);
								$certificates = explode("," , get_user_meta($member->user_id, "affiliate_certificates", true) );
								
								
								?>
								<tr id="pmpro_account-linked_members-<?php echo $member->user_id; ?>">
									<td>
										<?php echo $first_name." ".$last_name; ?>
									</td>
									<td>
										<?php echo $email; ?>
									</td>
									<td>
										<?php echo $phone; ?>
									</td>
									<td>
										<?php echo $member_ID; ?>
									</td>
									<td>
										<?php 
										
											if(count($certificates)){
												foreach($certificates as $certificate){
													$file_name = preg_replace("/.*\//","",$certificate);
													$file_name = str_replace("'","",$file_name);
													echo "<a href=".$certificate." target='_blank' class='affiliate_certificates_a' >".$file_name."</a>";
												}
											}
										
										?>
									</td>
								</tr>
							<?php 
							//}
						}
						
						
					}else {
						echo "<tr><td>No results</td></tr>";
					}
				
				?>
				</tbody>
			</table>

			<?php
				if( $q == "" ){
					echo pmpro_getPaginationString($pn, $totalmembers, $limit, 1, add_query_arg(array("elhash" => "kni_linked_members")), "&pn=");
				}
			?>
					
			
		</div> <!-- end pmpro_account-linked_members -->
		<?php } ?>
		
		
		
	</div> <!-- end pmpro_account -->		
	<?php
	
	}
	
	$content = ob_get_contents();
	ob_end_clean();
	
	echo $content;
}
add_action( 'woocommerce_account_my-memberships-endpoint_endpoint', 'my_memberships_endpoint_content' );

/*
	My tests tab 
*/
function tab_my_tests_content(){
	?>
		<div class="exam_container" >
			
			<?php if(pmpro_getMembershipLevelsForUser()[0]->id != 4) {?>
			<div class="exam_row" >
				<h2>Examiner</h2>
				<div class="exams" >
					<a class="btn" href="/examiner-online-test/" target="_blank" >Online test</a>
				</div>
				<div class="exams" >

					<a class="btn" href="/examiner-offline-test/" target="_blank">Offline test</a>
					<form method="post" >
						
					</form>
				</div>
			</div>
			<?php }else { echo "No tests."; }?>
				
		</div>
	<?
}
add_action( 'woocommerce_account_my-tests_endpoint', 'tab_my_tests_content' );

/*
	Account details tab 
*/
function kni_woocommerce_save_account_details( $user_id ) {
	
	global $member_levels_ids, $affiliate_levels_ids;
	
	update_user_meta( $user_id, 'billing_first_name', $_POST[ 'account_first_name' ] );
	update_user_meta( $user_id, 'billing_last_name', $_POST[ 'account_last_name' ] );
	wp_update_user( array( 'ID' => $user_id, 'display_name' => $_POST[ 'account_first_name' ]) );
	
	if( in_array(pmpro_getMembershipLevelsForUser()[0]->id, $affiliate_levels_ids) ){
		
		update_user_meta( $user_id, 'account_logo', htmlentities( $_POST[ 'account_logo' ] ) );
		
		//Physical address 
		update_user_meta( $user_id, 'billing_address_1', $_POST[ 'user_street_number' ].", ".$_POST[ 'user_street_name' ]);
		update_user_meta( $user_id, 'user_street_number', htmlentities( $_POST[ 'user_street_number' ] ) );
		update_user_meta( $user_id, 'user_street_name', htmlentities( $_POST[ 'user_street_name' ] ) );
		update_user_meta( $user_id, 'administrative_area_level_1', htmlentities( $_POST[ 'administrative_area_level_1' ] ) );
		update_user_meta( $user_id, 'billing_city', htmlentities( $_POST[ 'user_locality' ] ) );
		update_user_meta( $user_id, 'billing_country', htmlentities( $_POST[ 'user_country' ] ) );
		update_user_meta( $user_id, 'billing_postcode', htmlentities( $_POST[ 'user_postal_code' ] ) );
		update_user_meta( $user_id, 'user_physical_address', htmlentities( $_POST[ 'user_physical_address' ] ) );
		
		//PDF certificates
		update_user_meta( $user_id, 'affiliate_certificates', $_POST[ 'affiliate_certificates' ] );
		
		//Martial arts. Black belt number
		update_user_meta( $user_id, 'account_martial_arts_black_belt', $_POST[ 'account_martial_arts_black_belt' ] );

		$AMABB = json_decode( stripslashes($_POST[ 'account_martial_arts_black_belt' ]), true );
		if(count($AMABB)){
			foreach($AMABB as $k => $v){
				update_user_meta( $user_id, $k.'_black_belt_number', $_POST[ $k.'_black_belt_number' ] );
			}
		}		
		
		//Name of Affiliate
		wp_update_user( array( 'ID' => $user_id, 'user_url' => $_POST[ 'user_url' ], 'display_name' => $_POST[ 'billing_company' ]) );
		
	}else if( in_array(pmpro_getMembershipLevelsForUser()[0]->id, $member_levels_ids) ){
		update_user_meta( $user_id, 'account_martial_arts_method', stripslashes( $_POST[ 'account_martial_arts_method' ] ) );
		update_user_meta( $user_id, 'billing_phone', stripslashes( $_POST[ 'billing_phone' ] ) );
	}	
	
	
}
add_action( 'woocommerce_save_account_details', 'kni_woocommerce_save_account_details' );

add_filter( 'woocommerce_save_account_details_required_fields', function($args){
	
	global $member_levels_ids, $affiliate_levels_ids;
	
	if( in_array(pmpro_getMembershipLevelsForUser()[0]->id, $affiliate_levels_ids) ){
		
		$args["billing_company"] = __( 'Name', 'woocommerce' );
		$args["account_logo"] = __( 'Logo', 'woocommerce' );
		$args["user_url"] = __( 'Website', 'woocommerce' );
		$args["user_physical_address"] = __( 'Physical address', 'woocommerce' );
		
		$account_martial_arts_black_belt = json_decode(stripslashes($_POST["account_martial_arts_black_belt"]), true);
		if( count($account_martial_arts_black_belt) ){
			
			foreach($account_martial_arts_black_belt as $k => $v ){
				$args[$k."_black_belt_number"] = $k." black belt number";
				setcookie($k."_black_belt_number", "123", time()+3600);
			}
		} else {
			$args["account_martial_arts_black_belt"] = __( 'Martial arts in which the affiliate has a black belt', 'woocommerce' );
		}		

	} else if( in_array(pmpro_getMembershipLevelsForUser()[0]->id, $member_levels_ids) ){
		$account_martial_arts_method = json_decode(stripslashes($_POST["account_martial_arts_method"]), true);
		
		if( !count($account_martial_arts_method) ){
			$args["account_martial_arts_method"] = __( 'Martial Arts method', 'woocommerce' );
		}
		
		$args["billing_phone"] = __( 'Phone Number', 'woocommerce' );
		
	}	
	
	return $args;
});

function wooc_validate_custom_field( $args )
{
    if ( isset( $_POST['user_url'] ) ) 
    {
        if(filter_var($_POST['user_url'], FILTER_VALIDATE_URL) === FALSE) {
			 $args->add( 'error', __( '<b>URL</b> address is not valid!', 'woocommerce' ),'');
		}
       
    }
	
	if ( isset( $_POST['account_email'] ) ) 
    {
        if(filter_var($_POST['account_email'], FILTER_VALIDATE_EMAIL) === FALSE) {
			 $args->add( 'error', __( '<b>Email</b> address is not valid!', 'woocommerce' ),'');
		}
       
    }
	
	
}
add_action( 'woocommerce_save_account_details_errors','wooc_validate_custom_field', 10, 1 );


/* -Registration flow */
add_filter( "pmpro_required_billing_fields", function(){
	/*
	$pmpro_required_billing_fields = array(
		"AccountNumber"   => $AccountNumber,
		"ExpirationMonth" => $ExpirationMonth,
		"ExpirationYear"  => $ExpirationYear,
		"CVV"             => $CVV
	);
	*/
	$pmpro_required_billing_fields = array();
	return $pmpro_required_billing_fields;
} );

add_filter( "pmpro_required_user_fields", function(){
	$pmpro_required_user_fields = array();
	return $pmpro_required_user_fields;
} );

add_filter("pmpro_checkout_confirm_email", false);

add_filter( "pmpro_skip_account_fields", function($skip_account_fields, $current_user){
	$skip_account_fields = true;
	return $skip_account_fields;
});

add_filter('pmpro_include_billing_address_fields', true);

//Add Extra fields
add_action('pmpro_checkout_after_billing_fields', function(){
	global $pmpro_required_billing_fields, $pmpro_level, $member_levels_ids, $affiliate_levels_ids;

	
	if(!empty($_REQUEST['bfirstname']))	
		$bfirstname = $_REQUEST['bfirstname'];	
	else
		$bfirstname = "";
	if(!empty($_REQUEST['last_name']))
		$last_name = $_REQUEST['last_name'];
	else
		$last_name = "";
	if(!empty($_REQUEST['account_email']))
		$account_email = $_REQUEST['account_email'];
	else
		$account_email = "";
	if(!empty($_REQUEST['account_martial_arts_years']))
		$account_martial_arts_years = $_REQUEST['account_martial_arts_years'];
	else
		$account_martial_arts_years = "";
	if(!empty($_REQUEST['reg_as_affiliate']))
		$reg_as_affiliate = $_REQUEST['reg_as_affiliate'];
	else
		$reg_as_affiliate = "";
	if(!empty($_REQUEST['aff_name']))
		$aff_name = $_REQUEST['aff_name'];
	else
		$aff_name = "";
	if(!empty($_FILES['aff_logo']))
		$aff_logo = $_FILES['aff_logo'];
	else
		$aff_logo = "";
	if(!empty($_REQUEST['aff_url']))
		$aff_url = $_REQUEST['aff_url'];
	else
		$aff_url = "";
	if(!empty($_REQUEST['user_physical_address']))
		$user_physical_address = $_REQUEST['user_physical_address'];
	else
		$user_physical_address = "";
	if(!empty($_REQUEST['user_street_number']))
		$user_street_number = $_REQUEST['user_street_number'];
	else
		$user_street_number = "";
	if(!empty($_REQUEST['user_street_name']))
		$user_street_name = $_REQUEST['user_street_name'];
	else
		$user_street_name = "";
	if(!empty($_REQUEST['user_locality']))
		$user_locality = $_REQUEST['user_locality'];
	else
		$user_locality = "";
	if(!empty($_REQUEST['user_administrative_area_level_1']))
		$user_administrative_area_level_1 = $_REQUEST['user_administrative_area_level_1'];
	else
		$user_administrative_area_level_1 = "";
	if(!empty($_REQUEST['user_country']))
		$user_country = $_REQUEST['user_country'];
	else
		$user_country = "";
	if(!empty($_REQUEST['postal_code']))
		$postal_code = $_REQUEST['postal_code'];
	else
		$postal_code = "";	
	if(!empty($_REQUEST['account_martial_arts_black_belt']))
		$account_martial_arts_black_belt = $_REQUEST['account_martial_arts_black_belt'];
	else
		$account_martial_arts_black_belt = "";
	if(!empty($_REQUEST['affiliate_tc']))
		$affiliate_tc = $_REQUEST['affiliate_tc'];
	else
		$affiliate_tc = "";
	
	$pmpro_required_billing_fields['bfirstname'] = $bfirstname;
	$pmpro_required_billing_fields['last_name'] = $last_name;
	$pmpro_required_billing_fields['account_email'] = $account_email;
	$pmpro_required_billing_fields['account_martial_arts_years'] = $account_martial_arts_years;
	$pmpro_required_billing_fields['aff_name'] = $aff_name;
	$pmpro_required_billing_fields['aff_logo'] = $aff_logo;
	$pmpro_required_billing_fields['aff_url'] = $aff_url;
	$pmpro_required_billing_fields['user_physical_address'] = $user_physical_address;
	$pmpro_required_billing_fields['account_martial_arts_black_belt'] = $account_martial_arts_black_belt;
	$pmpro_required_billing_fields['affiliate_tc'] = $affiliate_tc;
	
	
	$martial_arts_methods = json_decode(stripslashes($account_martial_arts_years), true);
	
	if( count($martial_arts_methods) ){
		foreach( $martial_arts_methods as $k => $v ){
			
			if(!empty($_REQUEST[$k."_years_number"]))
				${$k."_years_number"} = $_REQUEST[$k."_years_number"];
			else
				${$k."_years_number"} = "";
						
			$pmpro_required_billing_fields[$k."_years_number"] = ${$k."_years_number"};
		}
	}
	
	$martial_arts_black_belt = json_decode(stripslashes($account_martial_arts_black_belt), true);
	
	if( count($martial_arts_black_belt) ){
		foreach( $martial_arts_black_belt as $k => $v ){
			
			if(!empty($_REQUEST[$k."_black_belt_number"]))
				${$k."_black_belt_number"} = $_REQUEST[$k."_black_belt_number"];
			else
				${$k."_black_belt_number"} = "";
						
			$pmpro_required_billing_fields[$k."_black_belt_number"] = ${$k."_black_belt_number"};
		}
	}
	
	?>
		<div id="pmpro_checkout_box-checkout_boxes" class="pmpro_checkout kni_checkout_box">
		
			<h4 >	
				<span class="pmpro_thead-name">Account Information</span>
			</h4>
			<div class="pmpro_checkout-fields">
								
				<div id="first_name_div" class="pmpro_checkout-field">
					<label for="bfirstname">First Name</label>					
					<input type="text" id="bfirstname" name="bfirstname" value="<?php echo esc_attr($bfirstname)?>" size="40" class="bfirstname input <?php echo pmpro_getClassForField("bfirstname");?>">
				</div>
				
				<div id="last_name_div" class="pmpro_checkout-field">
					<label for="last_name">Last Name</label>
					<input type="text" id="last_name" name="last_name" value="<?php echo esc_attr($last_name)?>" size="40" class="last_name input <?php echo pmpro_getClassForField("last_name");?> ">
				</div>
				
				<div id="bemail_div" class="pmpro_checkout-field">
					<label for="account_email">Email</label>
					<input type="hidden" name="bemail" id="kbemail" />
					<input type="hidden" name="bconfirmemail" id="kbconfirmemail" />
					<input type="email" id="account_email" name="account_email" value="<?php echo esc_attr($account_email)?>" size="40" class="bemail input <?php echo pmpro_getClassForField("account_email");?>">
					
					<script>
						jQuery(function($){
							$('#account_email').blur(function(){
								$('#kbemail, #kbconfirmemail').val($('#account_email').val());
							})
						})
					</script>
					
				</div>
				<?php if( in_array($pmpro_level->id, $member_levels_ids) ){ ?>
				<div id="account_martial_arts_years_div" class="pmpro_checkout-field">
					<label for="account_martial_arts_years" class="">Martial Arts method</label>
					<input type="hidden" name="account_martial_arts_years" value='<?php echo stripslashes($account_martial_arts_years)?>' >
					<select id="account_martial_arts_years" class="ui fluid search selection dropdown <?php echo pmpro_getClassForField("account_martial_arts_years");?>" multiple="">
						<option value="">Select a method...</option>
						<option value="KRT">Karate</option>
						<option value="UFC">UFC</option>
						<option value="AKD">Aikido</option>
						<option value="KNGF">Kong fu</option>
						<option value="JUDO">Judo</option>
						<option value="ARNIS">Arnis</option>
						<option value="KRVMG">Krav Maga</option>
						<option value="KPP">Kapap</option>
						<option value="MMA">MMA</option>
						<option value="NJTS">Ninjutsu</option>
						<option value="K1">K1</option>
						<option value="BJJ">Brazilian jiu-jitsu (Bjj)</option>
						<option value="TKWND">Taekwondo</option>
						<option value="CPR">Capoeira</option>
						<option value="ESKR">Eskrima</option>
						<option value="JJ">Jujutsu</option>
						<option value="PNKR">Pankration</option>
						<option value="AJ">Aiki Jutsu</option>
						<option value="KD">Kendo</option>
						<option value="SH">Shi Heun</option>
						<option value="KJ">Kempo Jitsu</option>
					</select>
					
					
					
					<script type="text/javascript">
						jQuery(function($){
							
							$('#account_martial_arts_years').dropdown('set selected',[<?php 						 
								if(count($martial_arts_methods)){
									foreach ( $martial_arts_methods as $k => $value ){
										echo "'".$value."',";
									}
								}
							?>]);
						
							$('#account_martial_arts_years').dropdown('setting', 'onChange',function(value, text, $choice) {					
			
								var jsonMAY = {};
								
								if($('.account_martial_arts_years_wrapper').length && value.length < 1 ){
									$('.account_martial_arts_years_wrapper').remove();
								}
								
								value.forEach(function(item, i, value) {
									jsonMAY[item] = $("#account_martial_arts_years option[value="+item+"]").text();
								});
								
								$('input[name=account_martial_arts_years]').val(JSON.stringify(jsonMAY));
								
							})
							
							$('#account_martial_arts_years').dropdown('setting', 'onAdd',function(addedValue, addedText, $addedChoice) {
								
								//unhighlight error field
								$('#account_martial_arts_years_div .pmpro_error').removeClass('pmpro_error');
								
								if(!$('.account_martial_arts_years_wrapper').length){
									$('#account_martial_arts_years_div').after('<div class="account_martial_arts_years_wrapper"><label>Years practicing martial arts</label><div class="martial_arts_years_number_wrapper" ><table width="100%" cellpadding="0" cellspacing="0" border="0"><thead><tr><th>Martial arts</th><th>Years number</th></tr></thead><tbody><tr class="tr_'+addedValue+'" ><td>'+addedText+'</td><td><input type="number" class="years_number pmpro_required" name="'+addedValue+'_years_number" min="0"  /><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span></td></tr></tbody></table></div></div>');
								} else{
									$('.account_martial_arts_years_wrapper tbody').append('<tr class="tr_'+addedValue+'" ><td>'+addedText+'</td><td><input type="number" class="years_number pmpro_required" name="'+addedValue+'_years_number"  min="0" /><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span></td></tr>')
								}						
								
							})
							
							$('#account_martial_arts_years').dropdown('setting', 'onRemove',function(removedValue, removedText, $removedChoice) {
	
								$('.account_martial_arts_years_wrapper .tr_'+removedValue).remove();
							})
							
						})
					</script>	  
					
				</div>
				
				<?php 			
					if(count($martial_arts_methods)){
				?>
					<div class="account_martial_arts_years_wrapper"><label>Years practicing martial arts</label>		
						<div class="martial_arts_years_number_wrapper" >
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<thead>
									<tr>
										<th>Martial arts</th>
										<th>Years number</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($martial_arts_methods as $k => $value){?>
									<tr class="tr_<?php echo $k; ?>" >
										<td><?php echo $value; ?></td>
										<td><input type="number" class="years_number <?php echo pmpro_getClassForField($k."_years_number"); ?>" name="<?php echo $k; ?>_years_number" value="<?php echo ${$k."_years_number"}; ?>" min="0" /></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>				
						</div>			
					</div>
				
				<?php 
					}
				}
				?>
				
				<!-- Only for Affilates -->
				<?php if( in_array($pmpro_level->id, $affiliate_levels_ids) ){ ?>				
				
				<div class="affiliate_fields_container" >
					<div id="aff_name_div" class="pmpro_checkout-field">
						<label for="aff_name">Affiliate Name</label>					
						<input type="text" id="aff_name" name="aff_name" value="<?php echo esc_attr($aff_name)?>" size="40" class="aff_name input <?php echo pmpro_getClassForField("aff_name"); ?>">
					</div>
					
					<div id="aff_logo_div" class="pmpro_checkout-field">
						<label for="aff_logo">Logo</label>		 			
						<input type="file" id="aff_logo" name="aff_logo" size="40" class="aff_logo input <?php echo pmpro_getClassForField("aff_logo"); ?>">
					</div>
					
					<div id="aff_url_div" class="pmpro_checkout-field">
						<label for="aff_url">Website</label>		 			
						<input type="url" id="aff_url" name="aff_url" value="<?php echo esc_attr($aff_url)?>" size="40" class="aff_url input <?php echo pmpro_getClassForField("aff_url"); ?>">
					</div>
					
					<div id="aff_adress_div" class="pmpro_checkout-field">
						<label for="user_physical_address"><?php _e( 'Physical address', 'woocommerce' ); ?> </label>
						<input type="text" name="user_physical_address" id="user_physical_address" value="<?php echo esc_attr( $user_physical_address ); ?>" onFocus="geolocate()" class="<?php echo pmpro_getClassForField("user_physical_address"); ?>" />					
						<input type="hidden" name="user_street_number" id="street_number" value="<?php echo esc_attr( $user_street_number ); ?>" >
						<input type="hidden" name="user_street_name" id="route" value="<?php echo esc_attr( $user_street_name ); ?>" >
						<input type="hidden" name="user_locality" id="locality" value="<?php echo esc_attr( $user_locality ); ?>" >
						<input type="hidden" name="user_administrative_area_level_1" id="administrative_area_level_1" value="<?php echo esc_attr( $user_administrative_area_level_1 ); ?>" >
						<input type="hidden" name="user_country"  id="country" value="<?php echo esc_attr( $user_country ); ?>" >
						<input type="hidden" name="user_postal_code" id="postal_code" value="<?php echo esc_attr( $user_postal_code ); ?>" >
						
						<script>			  
						  var placeSearch, autocomplete;
						  var componentForm = {
							street_number: 'short_name',
							route: 'long_name',
							locality: 'long_name',
							administrative_area_level_1: 'short_name',
							country: 'short_name',
							postal_code: 'short_name'
						  };

						  function initAutocomplete() {
							// Create the autocomplete object, restricting the search to geographical
							// location types.
							autocomplete = new google.maps.places.Autocomplete(
								/** @type {!HTMLInputElement} */(document.getElementById('user_physical_address')),
								{types: ['geocode']});

							// When the user selects an address from the dropdown, populate the address
							// fields in the form.
							autocomplete.addListener('place_changed', fillInAddress);
						  }
						  
						  
							  function fillInAddress() {
								// Get the place details from the autocomplete object.
								var place = autocomplete.getPlace();
								for (var component in componentForm) {
								  document.getElementById(component).value = '';
								  document.getElementById(component).disabled = false;
								}

								// Get each component of the address from the place details
								// and fill the corresponding field on the form.
								for (var i = 0; i < place.address_components.length; i++) {
								  var addressType = place.address_components[i].types[0];
								  if (componentForm[addressType]) {
									var val = place.address_components[i][componentForm[addressType]];
									document.getElementById(addressType).value = val;
								  }
								}
							
								
							  }
						  
						  // Bias the autocomplete object to the user's geographical location,
						  // as supplied by the browser's 'navigator.geolocation' object.
						  function geolocate() {
							if (navigator.geolocation) {
							  navigator.geolocation.getCurrentPosition(function(position) {
								var geolocation = {
								  lat: position.coords.latitude,
								  lng: position.coords.longitude
								};
								var circle = new google.maps.Circle({
								  center: geolocation,
								  radius: position.coords.accuracy
								});
								autocomplete.setBounds(circle.getBounds());
							  });
							}
						  }
						</script>
			   
						 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmOM_ubaLlIRhXiARVXVpJhXI9BTKcycw&libraries=places&callback=initAutocomplete"
					async defer></script>
					
					</div>
					
					<div id="aff_certif_div" class="pmpro_checkout-field">
						<label>PDF certificates</label>
						<div class="all_certif_btns" >
							<div class="aff_certif_container"><input type="file" name="aff_certif[]" size="40" class="aff_certif input <?php echo pmpro_getClassForField("aff_certif"); ?>" /> <span class="add">+</span> <span class="remove">-</span></div>
						</div>

						<script type="text/javascript" >
							jQuery(function($){
								$('body').on('click', '.add', function(){
									$(this).parents(".aff_certif_container").after('<div class="aff_certif_container"><input type="file" name="aff_certif[]" size="40" class="aff_certif input pmpro_required" /> <span class="add">+</span> <span class="remove">-</span></div>');
								})
								  
								$('body').on('click', '.remove', function(){
									if($("#aff_certif_div .aff_certif_container").length > 1){
										$(this).parents(".aff_certif_container").remove();
									}								
								})
							})
						</script>
						
					</div>
					
					<div class="pmpro_checkout-field kni_martial_arts_black_belt" id="account_martial_arts_black_belt_wrapper">
				
						<label for="account_martial_arts_black_belt" class=""><?php _e( 'Martial arts in which the affiliate has a black belt', 'woocommerce' ); ?></label>
						<input type="hidden" name="account_martial_arts_black_belt" value='<?php echo stripslashes($account_martial_arts_black_belt)?>' >
						<select id="account_martial_arts_black_belt" class="ui fluid search selection dropdown <?php echo pmpro_getClassForField("account_martial_arts_black_belt"); ?>" multiple="">
							<option value="">Select a method...</option>
							<option value="KRT">Karate</option>
							<option value="UFC">UFC</option>
							<option value="AKD">Aikido</option>
							<option value="KNGF">Kong fu</option>
							<option value="JUDO">Judo</option>
							<option value="ARNIS">Arnis</option>
							<option value="KRVMG">Krav Maga</option>
							<option value="KPP">Kapap</option>
							<option value="MMA">MMA</option>
							<option value="NJTS">Ninjutsu</option>
							<option value="K1">K1</option>
							<option value="BJJ">Brazilian jiu-jitsu (Bjj)</option>
							<option value="TKWND">Taekwondo</option>
							<option value="CPR">Capoeira</option>
							<option value="ESKR">Eskrima</option>
							<option value="JJ">Jujutsu</option>
							<option value="PNKR">Pankration</option>
							<option value="AJ">Aiki Jutsu</option>
							<option value="KD">Kendo</option>
							<option value="SH">Shi Heun</option>
							<option value="KJ">Kempo Jitsu</option>
						</select>
											
						<script type="text/javascript">
							jQuery(function($){
								
								$('#account_martial_arts_black_belt').dropdown('set selected',[<?php 						
									if(count($martial_arts_black_belt)){
										foreach ( $martial_arts_black_belt as $k => $value ){
											echo "'".$value."',";
										}
									}
								?>]);
								
								$('#account_martial_arts_black_belt').dropdown('setting', 'onChange',function(value, text, $choice) {					
													
									var jsonMABB = {};
									
									if($('.martial_arts_black_belt_number_wrapper').length && value.length < 1 ){
										$('.martial_arts_black_belt_number_wrapper').remove();
									}
									
									value.forEach(function(item, i, value) {
										jsonMABB[item] = $("#account_martial_arts_black_belt option[value="+item+"]").text();
									});
									
									$('input[name=account_martial_arts_black_belt]').val(JSON.stringify(jsonMABB));
									
								})
								
								$('#account_martial_arts_black_belt').dropdown('setting', 'onAdd',function(addedValue, addedText, $addedChoice) {
									
									//unhighlight error field
									$('#account_martial_arts_black_belt_wrapper .pmpro_error').removeClass('pmpro_error');
									
									if(!$('.martial_arts_black_belt_number_wrapper').length){
										$('#account_martial_arts_black_belt_wrapper').after('<div class="martial_arts_black_belt_number_wrapper"><label>Black belt number per each of the above martial arts</label><div class="martial_arts_belt_number_wrapper" ><table width="100%" cellpadding="0" cellspacing="0" border="0"><thead><tr><th>Martial arts</th><th>Black belt number</th></tr></thead><tbody><tr class="tr_'+addedValue+'" ><td>'+addedText+'</td><td><input type="text" class="belt_number pmpro_required" name="'+addedValue+'_black_belt_number"  /><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span></td></tr></tbody></table></div></div>');
									} else{
										$('.martial_arts_black_belt_number_wrapper tbody').append('<tr class="tr_'+addedValue+'" ><td>'+addedText+'</td><td><input type="text" class="belt_number pmpro_required" name="'+addedValue+'_black_belt_number"  /><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span></td></tr>')
									}						
									
								})
								
								$('#account_martial_arts_black_belt').dropdown('setting', 'onRemove',function(removedValue, removedText, $removedChoice) {
									$('.martial_arts_black_belt_number_wrapper .tr_'+removedValue).remove();
								})
								
							})
						</script>	  
						
					</div>
					
					<?php 			
						if(count($martial_arts_black_belt)){
					?>
						<div class=" martial_arts_black_belt_number_wrapper"><label>Black belt number per each of the above martial arts</label>		
							<div class="martial_arts_belt_number_wrapper" >
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<thead>
										<tr>
											<th>Martial arts</th>
											<th>Black belt number</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($martial_arts_black_belt as $k => $value){?>
										<tr class="tr_<?php echo $k; ?>" >
											<td><?php echo $value; ?></td>
										<td><input type="text" class="belt_number <?php echo pmpro_getClassForField($k.'_black_belt_number'); ?>" name="<?php echo $k; ?>_black_belt_number" value="<?php echo esc_attr( ${$k.'_black_belt_number'} ); ?>" /></td>
										</tr>
										<?php } ?>
									</tbody>
								</table>				
							</div>			
						</div>
					
					<?php 
						}
					?>
				</div>

				<?php } ?>
					<div id="affiliate_tc_div" class="pmpro_checkout-field">
						<label >Agree to <span>T&C </span></label>
						<input type="checkbox" id="affiliate_tc" name="affiliate_tc"  size="40" class="affiliate_tc input <?php echo pmpro_getClassForField('affiliate_tc'); ?>">
						
						<script type="text/javascript">
							jQuery(function($){
								
								$('#affiliate_tc_div label span').click(function(){
									$('.modal_box').fadeIn();
								})
								
								$('.modal_box .close, .modal_box .modal_background').click(function(){
									$('.modal_box').fadeOut();
								})
								
							})
						</script>
						
					</div>
				
			</div>
			
		</div> <!-- end pmpro_checkout-fields -->

	<?php
});

/*--Modal window for T&C */
add_action('pmpro_checkout_after_form', function(){
	?>
		<div class="modal_box"><div class="modal_background" ></div><div class="modal_container" ><div class="close" >X</div><div class="modal_content" >
	<?php 
		$page_id = get_id_by_slug('/about/terms-and-conditions');
				
		if( $page_id == null ){
			echo 'The "Terms and Conditions" page wasn\'t find. ' ;
		} else {
			$content_post = get_post($page_id); 
			$content = $content_post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			echo $content;
		}
	
	?>
		</div></div></div> 
	<?php 
});

add_action('pmpro_checkout_before_submit_button', function(){
	?>
		<div class="prev_next_btns_container" data-step="2">
			<div class="prev" ><span><</span> Prev</div>
			<div class="next" >Next <span>></span></div>
		</div>
		<script>
			jQuery(function($){
								
				$('.prev_next_btns_container div').click(function(){
					
					if($(this).hasClass('prev')){
						if( $('.prev_next_btns_container').attr('data-step') == 2 ){
							$(location).attr('href', $(location)[0].pathname);
						}else if( $('.prev_next_btns_container').attr('data-step') == 3 ){
							
							$('#pmpro_payment_information_fields, #promo_code_wrapper').fadeOut(function(){
								$('.pmpro_checkout.kni_checkout_box').fadeIn();
							});
							
							$('.prev_next_btns_container').attr('data-step', 2)
						}	
						
											
					}
					
					if($(this).hasClass('next')){
							//console.log('allReqFieldsIsFilled = '+allReqFieldsIsFilled('#pmpro_form','.pmpro_required'))
						if( allReqFieldsIsFilled('#pmpro_form','.pmpro_required') ){
							
						}
						
						if( $('.prev_next_btns_container').attr('data-step') == 2 ){
							$('.pmpro_checkout.kni_checkout_box').fadeOut(function(){
								$('#pmpro_payment_information_fields, #promo_code_wrapper').fadeIn();
							});
							
							$('.prev_next_btns_container').attr('data-step', 3)
							
						} else if( $('.prev_next_btns_container').attr('data-step') == 3 ){
							$('.pmpro_btn.pmpro_btn-submit-checkout').click();
						}				
										
					}
					
				})
				
				function allReqFieldsIsFilled(parentEl = '', reqClass = ''){					
					var e = 0;
					$(parentEl+' '+reqClass).each(function(){
						//console.log($(this))
						if(!$(this).val()){
							$(this).addClass('kni_error');
							e = 1;
						}
					})
					
					if(e){
						return false;
					}else {
						return true;
					}
					
				}
				
			})
			

		</script>
	<?php
});

function ajax_search_promo_code_db(){
	if ( !wp_verify_nonce( $_POST['security'],'kni-dkos0492ld' ) ){
		wp_die( 'Error');		
	}

	global $wpdb;
	
	$data = array();
	$level =  (object)$_POST['level'];
	$pmpro_affiliates_settings = $_POST['pmpro_affiliates_settings'];
	$aff_discount_member_reg = $pmpro_affiliates_settings['aff_discount_member_reg'];
	$aff_commission_member_reg = $pmpro_affiliates_settings['aff_commission_member_reg'];
	$aff_discount_affiliate_reg = $pmpro_affiliates_settings['aff_discount_affiliate_reg'];
	$aff_commission_affiliate_reg = $pmpro_affiliates_settings['aff_commission_affiliate_reg'];
	
	
	if($_POST['code']){
		
		//check if promo code exist
		$id = $wpdb->get_var("SELECT id FROM $wpdb->pmpro_affiliates WHERE code = '" . esc_sql($_POST['code']) . "' ");
		
		if($id){
			
			$data['success'] = "yes";

			//Find the discount
			if( in_array($level->id, $aff_discount_member_reg['levels']) ){
				
				if( $level->cycle_number > 1  ){
					$discount = ($level->cycle_number*$aff_discount_member_reg['value']);
				} else {
					$discount = $aff_discount_member_reg['value'];
				}				
				
			}else if( in_array($level->id, $aff_discount_affiliate_reg['levels']) ){
				
				if( $level->cycle_number > 1  ){
					$discount = ($level->cycle_number*$aff_discount_affiliate_reg['value']);
				} else {
					$discount = $aff_discount_affiliate_reg['value'];
				}		
				
			}
			
			$level->initial_payment = $level->initial_payment - $discount;
			
			if( $level->initial_payment < 0 ){
				$level->initial_payment = 0;
			}
			
			if($level->billing_amount > 0 && $level->cycle_number > 0 ){				
				$level->billing_amount = $level->billing_amount - $discount;
			}
			
			$data['discount'] = $discount;
			$data["success_message"] = "Discount code applied successfully!";
			$data["level_cost_message"] = pmpro_getLevelCost($level);
		
			
		}else {
			$data['error'] = "The discount code could not be found.";
		}
		
	} else {
		$data['error'] = "Enter your promo code.";
	}
	

	echo json_encode($data);
	
	die();
}

add_action('wp_ajax_kni_search_promocode', 'ajax_search_promo_code_db');
add_action('wp_ajax_nopriv_kni_search_promocode', 'ajax_search_promo_code_db');


//require the fields
function my_pmpro_registration_checks()
{

	global $pmpro_msg, $pmpro_msgt, $current_user, $pmpro_error_fields, $member_levels_ids, $affiliate_levels_ids;
	
	$ma_names = array();
	/*
	$bfirstname = $_REQUEST['bfirstname'];
	$bfirstname = $bfirstname;
	$last_name = $_REQUEST['last_name'];
	$account_email = $_REQUEST['account_email'];*/
	$account_martial_arts_years = json_decode(stripslashes($_REQUEST['account_martial_arts_years']), true);
	$account_martial_arts_black_belt = json_decode(stripslashes($_REQUEST['account_martial_arts_black_belt']), true);
	
	if(empty($_REQUEST['bfirstname'])){
		$ma_names[] = "<strong>First Name</strong>";
		$pmpro_error_fields[]= 'bfirstname';
	} 
	
	if(empty($_REQUEST['last_name'])){
		$ma_names[] = "<strong>Last Name</strong>";
		$pmpro_error_fields[]= 'last_name';
	}
	
	if(empty($_REQUEST['account_email'])){
		$ma_names[] = "<strong>Email</strong>";
		$pmpro_error_fields[]= 'account_email';
	} else {
				
		$valid = filter_var($_REQUEST['account_email'], FILTER_VALIDATE_EMAIL);
		
		if(!$valid){
			$pmpro_error_fields[]= 'account_email';
		}
		
	}
	
	if( in_array($_REQUEST['level'], $member_levels_ids) ){
	
		if(!count($account_martial_arts_years)){
			$ma_names[] = "<strong>Martial Arts method</strong>";
			$pmpro_error_fields[]= 'account_martial_arts_years';
		}
		

		if( count($account_martial_arts_years) ){
			foreach( $account_martial_arts_years as $k => $v ){
				
				${$k."_years_number"} = $_REQUEST[$k."_years_number"];
				
				if(empty($_REQUEST[$k."_years_number"])){
					$ma_names[] = "<strong>".$v."</strong>";
					$pmpro_error_fields[]=$k."_years_number";
				}
				
			}
		}
	
	}
	
	if( in_array($_REQUEST['level'], $affiliate_levels_ids) ){
		if(empty($_REQUEST['aff_name'])){
			$ma_names[] = "<strong>Name</strong>";
			$pmpro_error_fields[]= 'aff_name';
		}
		
		if(empty($_FILES['aff_logo']['name'])){
			$ma_names[] = "<strong>Logo</strong>";
			$pmpro_error_fields[]= 'aff_logo';
		}else {
			$allowed =  array('gif','png' ,'jpg','jpeg','JPG','JPEG', 'GIF','PNG');
			$filename = $_FILES['aff_logo']['name'];
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if(!in_array($ext,$allowed) ) {
				$logo_w_ext = "<strong>Logo</strong>: wrong image extension (allowed extensions: 'gif','png' ,'jpg','jpeg'). ";
				$pmpro_error_fields[]= 'aff_logo';
				
			}
			
			
		}
		
		if(empty($_REQUEST['aff_url'])){
			$ma_names[] = "<strong>Website</strong>";
			$pmpro_error_fields[]= 'aff_url';
		} else {
					
			$valid = preg_match_all('#[-a-zA-Z0-9@:%_\+.~\#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~\#?&//=]*)?#si', $_REQUEST['aff_url']);
			
			if(!$valid){
				$pmpro_error_fields[]= 'aff_url';
			}
			
		}
		
		if(empty($_REQUEST['user_physical_address'])){
			$ma_names[] = "<strong>Physical address</strong>";
			$pmpro_error_fields[]= 'user_physical_address';
		}
		
		if(count($_FILES['aff_certif']['name'])){
			$allowed =  array('pdf', 'PDF');
			$file = $_FILES['aff_certif'];
			foreach($file['name'] as $k => $v ){
				if(!empty($v)){
					$ext = pathinfo($v, PATHINFO_EXTENSION);
					if(!in_array($ext,$allowed) ) {
						$pmpro_error_fields[]= 'aff_certif';
						$cert_w_ext = "<strong>PDF certificates</strong>: wrong file extension (allowed extensions: 'pdf'). ";
					}
				}
				
			}

		}
		
		
		if(!count($account_martial_arts_black_belt)){
			$ma_names[] = "<strong>Martial arts in which the affiliate has a black belt</strong>";
			$pmpro_error_fields[]= 'account_martial_arts_black_belt';
		}
		
		if( count($account_martial_arts_black_belt) ){
			foreach( $account_martial_arts_black_belt as $k => $v ){
				
				${$k."_black_belt_number"} = $_REQUEST[$k."_black_belt_number"];
				
				if(empty($_REQUEST[$k."_black_belt_number"])){
					$ma_names[] = "<strong>".$v."</strong>";
					$pmpro_error_fields[]=$k."_black_belt_number";
				}
				
			}
		}
		
		if(empty($_REQUEST['affiliate_tc'])){
			$ma_names[] = "<strong>Agree to T&C</strong>";
			$pmpro_error_fields[]= 'affiliate_tc';
		}
	}
	
	
 
	if( !count($ma_names) || $current_user->ID){
		//all good
		return true;
	}else {
		//$pmpro_msg = "The ".implode(", ", $ma_names)." fields are required.";
		$pmpro_msg = "Please fill all required fields. ".$logo_w_ext.$cert_w_ext;
		$pmpro_msgt = "pmpro_error";			
		
		return false;
	}
	
}
add_filter("pmpro_registration_checks", "my_pmpro_registration_checks");

//Save extra fields
add_action('pmpro_after_checkout', function($user_id){
	
	global $member_levels_ids, $affiliate_levels_ids;
	
	
	//Genereal fields
	if(isset($_REQUEST['bfirstname'])){
		$bfirstname = $_REQUEST['bfirstname'];	
		
	}elseif(isset($_SESSION['bfirstname'])){
		$bfirstname = $_SESSION['bfirstname'];	
		unset($_SESSION['bfirstname']);
		
	}

	if(isset($_REQUEST['last_name'])){
		$last_name = $_REQUEST['last_name'];		
	}elseif(isset($_SESSION['last_name'])){
		$last_name = $_SESSION['last_name'];		
		unset($_SESSION['last_name']);
	}
	
	if(isset($_REQUEST['account_email'])){
		$account_email = $_REQUEST['account_email'];		
	}elseif(isset($_SESSION['account_email'])){
		$account_email = $_SESSION['account_email'];		
		unset($_SESSION['account_email']);
		
	}
	
	if(isset($bfirstname))	
		update_user_meta($user_id, "first_name", $bfirstname);
	if(isset($last_name))	
		update_user_meta($user_id, "billing_last_name", $last_name);
	if(isset($account_email))	
		wp_update_user( array( 'ID' => $user_id, 'user_email' => $account_email) );

	update_user_meta($user_id, "ima_member_id", get_memberID());
	
	//For Members
	if( in_array($_REQUEST['level'], $member_levels_ids) ){
		if(count(json_decode(stripslashes($_REQUEST['account_martial_arts_years']), true))){
			
			$account_martial_arts_years = $_REQUEST['account_martial_arts_years'];		
			
		}else if(count(json_decode(stripslashes($_SESSION['account_martial_arts_years']), true))){
			
			$account_martial_arts_years =$_SESSION['account_martial_arts_years'];		
			unset($_SESSION['account_martial_arts_years']);
			
		}
		
		
		if(isset($account_martial_arts_years))	
		update_user_meta($user_id, "account_martial_arts_method", $account_martial_arts_years);
		
		if( count(json_decode(stripslashes($account_martial_arts_years), true)) ){
			foreach( json_decode(stripslashes($account_martial_arts_years), true) as $k => $v ){
				
				if(isset($_REQUEST[$k."_years_number"])){
					${$k."_years_number"} = $_REQUEST[$k."_years_number"];		
				}elseif(isset($_SESSION[$k."_years_number"])){
					${$k."_years_number"} = $_SESSION[$k."_years_number"];	
					unset($_SESSION[$k."_years_number"]);				
				}
				
				if(isset(${$k."_years_number"}))	{
					update_user_meta($user_id, $k."_years_number", ${$k."_years_number"});
				}
				
			}
		}
		
		
	}
	
	
	//For Affilates
	if( in_array($_REQUEST['level'], $affiliate_levels_ids) ){	
		if(isset($_REQUEST['aff_name'])){
			$aff_name = $_REQUEST['aff_name'];		
		}elseif(isset($_SESSION['aff_name'])){
			$aff_name = $_SESSION['aff_name'];		
			unset($_SESSION['aff_name']);		
		}
		
		if(isset($_FILES['aff_logo']['name'])){
			$aff_logo = wp_get_attachment_url( implode(upload_user_files( 'aff_logo' ) ) );	
		}
		
		if(isset($_REQUEST['aff_url'])){
			$aff_url = $_REQUEST['aff_url'];		
		}elseif(isset($_SESSION['aff_url'])){
			$aff_url = $_SESSION['aff_url'];		
			unset($_SESSION['aff_url']);		
		}
		
		if(isset($_REQUEST['user_physical_address'])){
			$user_physical_address = $_REQUEST['user_physical_address'];		
		}elseif(isset($_SESSION['user_physical_address'])){
			$user_physical_address = $_SESSION['user_physical_address'];		
			unset($_SESSION['user_physical_address']);		
		}
		
		if(isset($_REQUEST['user_street_number'])){
			$user_street_number = $_REQUEST['user_street_number'];		
		}elseif(isset($_SESSION['user_street_number'])){
			$user_street_number = $_SESSION['user_street_number'];		
			unset($_SESSION['user_street_number']);		
		}
		
		if(isset($_REQUEST['user_street_name'])){
			$user_street_name = $_REQUEST['user_street_name'];		
		}elseif(isset($_SESSION['user_street_name'])){
			$user_street_name = $_SESSION['user_street_name'];		
			unset($_SESSION['user_street_name']);		
		}
		
		if(isset($_REQUEST['user_locality'])){
			$user_locality = $_REQUEST['user_locality'];		
		}elseif(isset($_SESSION['user_locality'])){
			$user_locality = $_SESSION['user_locality'];		
			unset($_SESSION['user_locality']);		
		}

		if(isset($_REQUEST['user_administrative_area_level_1'])){
			$user_administrative_area_level_1 = $_REQUEST['user_administrative_area_level_1'];		
		}elseif(isset($_SESSION['user_administrative_area_level_1'])){
			$user_administrative_area_level_1 = $_SESSION['user_administrative_area_level_1'];		
			unset($_SESSION['user_administrative_area_level_1']);		
		}
		
		if(isset($_REQUEST['user_country'])){
			$user_country = $_REQUEST['user_country'];		
		}elseif(isset($_SESSION['user_country'])){
			$user_country = $_SESSION['user_country'];		
			unset($_SESSION['user_country']);		
		}
		
		if(isset($_REQUEST['user_postal_code'])){
			$user_postal_code = $_REQUEST['user_postal_code'];		
		}elseif(isset($_SESSION['user_postal_code'])){
			$user_postal_code = $_SESSION['user_postal_code'];		
			unset($_SESSION['user_postal_code']);		
		}
		
		if(count($_FILES['aff_certif']['name'])){
			$certificates = array();
			$aids = upload_user_files( 'aff_certif', true );
			if( count($aids) ){
				foreach( $aids as $k => $v){
					$certificates[] = wp_get_attachment_url( $v );
				}
				
				$aff_cert = implode(",", $certificates);
				
			}			
		}
		
		if(count(json_decode(stripslashes($_REQUEST['account_martial_arts_black_belt']), true))){
			
			$account_martial_arts_black_belt = $_REQUEST['account_martial_arts_black_belt'];		
			
		}else if(count(json_decode(stripslashes($_SESSION['account_martial_arts_black_belt']), true))){
			
			$account_martial_arts_black_belt =$_SESSION['account_martial_arts_black_belt'];		
			unset($_SESSION['account_martial_arts_black_belt']);
			
		}
		
		
		
		if(isset($aff_name))	
			wp_update_user( array( 'ID' => $user_id, 'display_name' => $aff_name ) );
			update_user_meta($user_id, "billing_company", $aff_name);
		if(isset($aff_logo))	
			update_user_meta($user_id, "account_logo", $aff_logo);
		if(isset($aff_url))	
			wp_update_user( array( 'ID' => $user_id, 'user_url' => $aff_url ) );
		if(isset($user_street_number) || isset($user_street_name))	
			update_user_meta($user_id, "billing_address_1", stripslashes($user_street_number).', '.stripslashes($user_street_name));
		if(isset($user_street_number))	
			update_user_meta($user_id, "user_street_number", stripslashes($user_street_number));
		if(isset($user_street_name))	
			update_user_meta($user_id, "user_street_name", stripslashes($user_street_name));
		if(isset($user_administrative_area_level_1))	
			update_user_meta($user_id, "administrative_area_level_1", stripslashes($user_administrative_area_level_1));
			update_user_meta($user_id, "billing_state", stripslashes($user_administrative_area_level_1));
		if(isset($user_locality))	
			update_user_meta($user_id, "billing_city", stripslashes($user_locality));
		if(isset($user_country))	
			update_user_meta($user_id, "billing_country", stripslashes($user_country));
		if(isset($user_postal_code))	
			update_user_meta($user_id, "billing_postcode", stripslashes($user_postal_code));
		if(isset($user_physical_address))	
			update_user_meta($user_id, "user_physical_address", stripslashes($user_physical_address));
		
		if(isset($aff_cert))	
			update_user_meta($user_id, "affiliate_certificates", $aff_cert);
		
		if(isset($account_martial_arts_black_belt))	
			update_user_meta($user_id, "account_martial_arts_black_belt", $account_martial_arts_black_belt);
		
		if( count(json_decode(stripslashes($_REQUEST['account_martial_arts_black_belt']), true)) ){
			foreach( json_decode(stripslashes($_REQUEST['account_martial_arts_black_belt']), true) as $k => $v ){
				
				if(isset($_REQUEST[$k."_black_belt_number"])){
					${$k."_black_belt_number"} = $_REQUEST[$k."_black_belt_number"];		
				}elseif(isset($_SESSION[$k."_black_belt_number"])){
					${$k."_black_belt_number"} = $_SESSION[$k."_black_belt_number"];	
					unset($_SESSION[$k."_black_belt_number"]);				
				}
				
				if(isset(${$k."_black_belt_number"}))	{
					update_user_meta($user_id, $k."_black_belt_number", ${$k."_black_belt_number"});
				}
				
			}
		}		
	
	}
	
});


/*
 * Display Member ID in user profile
  */
add_action("pmpro_after_membership_level_profile_fields", "add_memberID_toprofile");
function add_memberID_toprofile($user){
	?>
		<table class="form-table">
			<tbody>
				<tr>
					<th><label>Member ID:</label></th> <td><?php echo get_user_meta($user->ID, "ima_member_id", true); ?></td>
				</tr>
			</tbody>
		</table>
	<?php 
}

/**
 * Upload files from frontend;
 */ 
function upload_user_files( $file_i, $multiple = false ){
	
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/media.php' );
	
	$file_return = array();
	
	if($multiple){
		
		$u_files = $_FILES[$file_i];
	
		foreach ($u_files['name'] as $key => $value) {
		  if ($u_files['name'][$key]) {
			$file = array(
			  'name'     => $u_files['name'][$key],
			  'type'     => $u_files['type'][$key],
			  'tmp_name' => $u_files['tmp_name'][$key],
			  'error'    => $u_files['error'][$key],
			  'size'     => $u_files['size'][$key]
			);
			
			$_FILES['kni_file'] = $file;
						
			$file_return[] = media_handle_upload( 'kni_file', 0);
				
		  }
		}
		
		if( !is_wp_error( $file_return ) ){
			return $file_return;
		}
		
	}else {
		$file_return[] = media_handle_upload( $file_i, 0);
		
		if( !is_wp_error( $file_return[0] ) ){
			return $file_return;
		}
		
	}
	
	return false;	
}

/*
 * Send members password through email after checkout. 
 * Use PMPro Email Templates shortcode !!password!! in checkout emails.
 * Remove password fields using custom checkout template to auto generate password.
*/
function my_generate_passwords()
{
	if(!empty($_REQUEST['bfirstname']) && empty($_REQUEST['password']))
	{
		$_REQUEST['password'] = pmpro_getDiscountCode() . pmpro_getDiscountCode();	//using two random discount codes
		$_REQUEST['password2'] = $_REQUEST['password'];
	}
}
add_action("init", "my_generate_passwords");

function my_email_password()
{
	global $gateway;
	
	$password = $_REQUEST['password'];
	$bfirstname = $_REQUEST['bfirstname'];
	
	$transient_var = $bfirstname."_password";
	
	set_transient( $transient_var, $password, HOUR_IN_SECONDS);
}

add_action('pmpro_after_checkout', 'my_email_password');

function my_pmpro_paypalexpress_session_vars()
{
	$bfirstname = $_SESSION['pmpro_signup_bfirstname'];
	$password = $_SESSION['pmpro_signup_password'];
	
	$transient_var = $bfirstname."_password";
	set_transient( $transient_var, $password, HOUR_IN_SECONDS);
}

add_action('pmpro_paypalexpress_session_vars', 'my_pmpro_paypalexpress_session_vars');

function my_pmpro_email_data($data, $email)
{
	$password = get_transient($data['user_login'].'_password');
	
	if($password != false)
		$data['password'] = $password;

	return $data;
}
add_filter("pmpro_email_data", "my_pmpro_email_data", 10, 2);


/*
  Hide Member Content From Searches.
*/

function my_pmpro_posts_results($results)
{
	//don't do anything in the admin
	if(is_admin())
		return $results;

	if(!empty($results) && !is_single() && !is_page())
	{		
		$newresults = array();
		foreach($results as $result)
		{
			if(pmpro_has_membership_access($result->ID))
			{			
				$newresults[] = $result;
			} else {
				$result->post_content = '';
				$newresults[] = $result;
			}
		}
		
		$results = $newresults;
		unset($newresults);
	}
			
	return $results;
}
add_filter('posts_results','my_pmpro_posts_results');

/**
 * get_id_by_slug('any-page-slug');
 */
function get_id_by_slug($page_slug) {
	$page = get_page_by_path($page_slug);
	if ($page) {
		return $page->ID;
	} else {
		return null;
	}
}

/*
 * Member ID generator
 */
function get_memberID( $seed = null ){ 
	global $wpdb;

	while(empty($code))
	{
		
		$code = mt_rand ( 1000000 , 10000000000 );
		$check = $wpdb->get_var("SELECT `user_id` FROM $wpdb->usermeta WHERE meta_value = '" . esc_sql($code) . "' LIMIT 1");
		
		if(!empty($check)){
			//echo "This code is'n unique";
			$code = NULL;
		}
		
		
	}

	return $code;
}


