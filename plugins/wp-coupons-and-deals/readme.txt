
=== WP Coupons and Deals ===
Contributors: imtiazrayhan
Tags: coupon, coupons, deal, deals, shortcode, coupon widget
Requires at least: 4.6
Tested up to: 4.8
Stable tag: 2.3.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Very lightweight and easy to use WordPress Coupons and deals plugin. Generate more affiliate sales with coupon codes and deals.

=== Description ===

**Create beautiful coupon and deal shortcode in your WordPress site and generate more affiliate sales.**

== Key Features ==
* Very lightweight, loads fast.
* Responsive and attractive shortcode.
* Live Preview of the coupon as you create it.
* Insert shortcode directly from the post editor.
* Click to copy functionality.
* Widget to show coupons and deals on widget areas.
* Easy to use settings.
* Coupon categories to categorize your coupons.
* Expiration system to show coupon expiration.
* Stylesheets and scripts are loaded conditionally for better performance.

== Pro Features ==
* Hide Coupons.
* Coupon expiration countdown.
* Coupon Templates.
* Show Specific Coupons.
* Show Coupons of specific Category.
* Coupons Archive Page.

**You can upgrade to Pro Version from your dashboard.** [__More Info and Demo of Pro Version__](https://wpcouponsdeals.com)

WP Coupons and Deals is a Coupon plugin that lets you add coupons, deals to your posts, pages with a simple and beautiful shortcode.

You can also show coupons and deals on your sidebar or other widget areas using an widget.

You can create two types of coupon. Coupon and Deal.

[__Live Demo__](https://wpcouponsdeals.com)

The coupon shortcode comes with the functionality to **click to copy coupon**. That means when users click on the coupon code, the coupon code will be copied to clipboard.


When coupon is copied, a link is opened in a new tab. You can use your affiliate link here. So when users copy the coupon code, your affiliate link is opened in a new tab.
This way you can generate more sales from your coupon codes.

Deal shortcode looks the same as the coupon type. Only difference is instead of coupon code, a button is showed which can say anything you want, like - 'Get This Deal', 'Claim This Deal'.

Also, when users click on this button, nothing is copied. Only your link opens in a new tab.

With both the coupon types, a discount amount/text is shown on the left, so users immediately know what the coupon code/deal is about. Below the discount amount/text coupon type is shown i.e whether it is a coupon code or a deal.


Both coupon code/deal button shows a little tooltip text to make it more attractive. You can set what text is showed here.

Expiration date or text is shown on the right below the coupon code/deal button. If coupon/deal is expired, an expired message is shown. You can show your own text here.
If any coupon/deal doesn't expire, another text is shown. You can customize that too.

You can customize the date format of the expiration date. Make sure you update expiration date of your old coupons, otherwise the previous date format will be shown.

Now you can also show only the coupon code instead of the full coupon with details. You can select the shortcode type from the shortcode inserter.

== How To Use ==

Here's a video to quickly show you how the plugin works.

[youtube https://www.youtube.com/watch?v=w_2_ONfWGvM]

After you have installed the plugin, you can see 'Coupons' with a little scissor on your admin dashboard menu.
Under that section, there are four items - Coupons, Add New Coupon, Coupon Categories, Settings.

1. Coupons shows the list of all coupons you have added. The list shows options like Coupon type, Coupon Code, Description, Link, Category, Shortcode, Expires.
2. Add New Coupon is where you'll add new coupons.
3. Coupon Categories shows the categories, You can also create categories here.
4. Settings contains some general settings you can set.

After you have added a new coupon, you have to insert a shortcode in your posts to show the coupon. You can find the shortcode from the coupons list page. You can copy from there and paste on post editor.
The easiest way is to insert the shortcode from the post editor. When you're on the post editor, you can see an 'Add Coupon' button beside the Add Media button.

Click on that, an window pops up. A list of all your added coupon is showed. There select the coupon code you want to insert and choose the shortcode type. Then click on 'Insert Coupon Shortcode' button.
Shortcode will be inserted on your post with corresponding ID. Now save your post and you can see your shortcode on your post.

You're done!

== Requirements ==

Please make sure you have:

1. PHP 5.2 or higher installed on your server.
2. WordPress version 4.5 or higher.

== Frequently Asked Questions ==

1. Coupon is not showing properly. What should I do?
    You can try clearing your cache.

2. Why there is no expiration message showing?
    Most probably you didn't choose to show it when you created the coupon. If you chose to show expiration and left the date field blank, 'Doesn't Expire' message is shown instead.

== Installation ==

This is one way you can install the plugin:

1. Download the plugin from WordPress plugin repository and upload all the files from wp-coupons-deals.zip into a folder within the /wp-content/plugins/ directory.
2. Activate the plugin through the  'Plugins ' menu in WordPress.

Another way :

1. From WordPress dashboard, go to Add New under Plugins section.
2. Search for  'WP Coupons and Deals'
3. From search results, click on  'WP Coupons and Deals'.
4. Click on Install button and activate the plugin.

== Screenshots ==
1. Example Shortcode output.
2. Widget Output.
3. Adding a new Coupon Category.
4. Adding a New Coupon Code.
5. Adding a New Deal.
6. Adding expiration dates.
7. Coupons list with information.
8. Adding coupon from post editor.
9. Inserting the Coupon Shortcode.
10. Shortcode in Post.
11. Adding the widget.

== Changelog ==

= 2.3.1 =

* Added - Translation for German. Thanks to Christian Bramer from webwidoo.com
* Fixed - Conflict with WooCommerce settings. Thanks to Shreyans Jain for reporting this.
* Fixed - Little Conflict in Safari browser.
* Fixed - Coupon copy conflict due to JS minification.
* Fixed - Some CSS bugs.
* Pro - Added - Option to insert category, archive shortcode from shortcode inserter in post editor.
* Pro - Added - Horizontal style for both archive and category shortcode using the default template of the single coupon.
* Pro - Added - Option to show affiliate link in the pop-up shown after user click to 'Show code'.
* Pro - Added - Revealing the coupon code after user have clicked on 'Show Code'.
* Pro - Added - Category filter when adding new coupon using the shortcode inserter in post editor.
* Pro - Added - Category filter and search option when adding widgets.
* Pro - Fixed - Pop-up not opening in Safari and Chrome on iOS.
* Pro - Fixed - Pop-up display conflict in Safari.
* Pro - Fixed - Some minor improvents on the back-end.

= 2.3.0 =

* Added - Translation for Vietnamese Language.
* Added - More/Less link to coupon description when description is long.
* Added - Option to customize the words count to add More/Less link.
* Added - Option to customize expire text.
* Added - Option to customize expired text.
* Added - New expire date format - dd-mm-yyyy.
* Added - A better way to copy coupon codes. Custom function that works with all browsers.
* Added - Knowledge base link in settings page.
* Added - Plugin version number to scripts register to avoid caching issue on plugin update.
* Fixed - Datepicker issue in Safari.
* Fixed - Coupon copy issue when same coupon appears twice on a page.
* Fixed - jQuery loading issue in widget.
* Fixed - CSS issue with the coupon code, not laoding correctly on all screen sizes.
* Updated - Minor CSS improvements.
* Updated - Plugin is now fully translation ready.
* Deprecated - Clipboard.Js library for coupon copy. It was conflicting on some browsers.
* Pro - Added - New Colorpicker to customize color of few elements.
* Pro - Added - Widget templates corresponding to shortcode templates. Whatever template you choose for the shortcode, will work on widgets too.
* Pro - Added - Two new settings tab in settings page. Hide Coupon settings and Pop-up settings.
* Pro - Added - Functionality to update countdown in preview when date or time is selected.
* Pro - Added - Option to customize Hidden coupon button text.
* Pro - Added - Option to customize Hidden coupon button hover text.
* Pro - Added - Option to customize hidden coupon button color.
* Pro - Added - Pop-up - copy button text customization option.
* Pro - Added - Pop-up - after copy button text customization option.
* Pro - Added - Pop-up - option to select whether to show description or custom text below the coupon code.
* Pro - Added - Pop-up - option to change coupon code background color.
* Pro - Added - Pop-up - option to change copy button background color.
* Pro - Fixed - CSS issue with coupon pop-up.
* Pro - Fixed - Issue with multple countdown on same page.
* Pro - Fixed - Error while using the free trial.
* Pro - Updated - Minor CSS improvements.
* Pro - Updated - Improvement in archive shortcode and category shortcode.

= 2.2.1 =

* Minor bug fixes.

= 2.2 =

* Pro version released.
* Some minor improvements.

= 2.1.3 =

* Added - Freemius SDK for opt in to track plugin stats to help improve it.
* Added - Support Forum link under Coupons Menu item.
* Some other minor improvements.

= 2.1.2 =

* Fixed - a bug on shortcode inserter button.
* Fixed - a bug on settings page stylesheet.

= 2.1.1 =

* Fixed - A bug, breaking backend popups.

= 2.1 =

* Fixed - a conflict with the post lock dialog box.

= 2.0 =

* Added - a coupon preview box to show live preview of coupon as you create it.
* Added - a new metabox to show the shortcodes when coupon is published.
* Added - custom admin notices to show the shortcodes when coupon is published.
* Added - links to admin toolbar so users can access them from anywhere.
* Added - an introduction page to show when plugin is activated.
* Fixed - a conflict with Visual Composer back end editor.

= 1.4 =

* Added - the shortcode to show only the coupon code.
* Added - the option to insert only coupon code shortcode from inserter.
* Added - the coupon only shortcode to admin columns in coupons list.

= 1.3.2 =

* Fixed - an expiration date format problem.

= 1.3.1 =

* Fixed - font size problem.

= 1.3 =

* Added the option to change date format for the expiration date.
* Fixed the issue of opening two links if same coupon exists in post and widget in the same page.

= 1.2 =

* Added widget functionality.
* Added conditional loading for scripts.
* Added template loader to load widget and shortcode.
* Minor code improvements.

= 1.1 =

* Few minor improvements.
* Changed the shortcode design.
* Settings page improvement.

= 1.0 =

* Initial Release
