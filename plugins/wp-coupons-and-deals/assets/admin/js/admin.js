// Shows or hides fields according to user inputs.
jQuery(document).ready(function($) {
    var button_text = $('#buttontext');
    var deal_text = $('#dealtext');
    var show_expiration = $('#show-expiration');
    var expiration = $('#expiredate');
    var time_expiration = $('#expiretime');
    var coupon_template = $('#coupon-template');
    var hide_coupon = $('#hide-coupon');
    var coupon_hidden = $('.wpcd-coupon-hidden');
    var coupon_not_hidden = $('.wpcd-coupon-not-hidden');

    if ($('#coupon-type').val() === 'Coupon') {
        button_text.show();
        deal_text.hide();
    } else {
        button_text.hide();
        deal_text.show();
    }
    $('[name="coupon-type"]').on('change', function() {
        if ($(this).val() === 'Coupon') {
            button_text.show("slow");
            deal_text.hide();
        } else {
            button_text.hide();
            deal_text.show("slow");
        }
    });

    if ($('#show-expiration').val() === 'Show') {
        expiration.show();
    } else {
        expiration.hide();
    }
    $('[name="show-expiration"]').on('change', function() {
        if ($(this).val() === 'Show') {
            expiration.show("slow");
        } else {
            expiration.hide();
        }
    });

    if (coupon_template.val() === 'Template Two') {
        time_expiration.show();
        expiration.show();
        show_expiration.hide();
    } else {
        time_expiration.hide();
    }

    $(coupon_template).on('change', function() {
        if ($(this).val() === 'Template Two') {
            time_expiration.show("slow");
            expiration.show("slow");
            show_expiration.hide();
        } else {
            time_expiration.hide();
            show_expiration.show();
        }
    });

    if (hide_coupon.val() === 'Yes') {
        coupon_hidden.show();
        coupon_not_hidden.hide();
    } else {
        coupon_hidden.hide();
        coupon_not_hidden.show();
    }

    $(hide_coupon).on('change', function() {
        if ($(this).val() === 'Yes') {
            coupon_hidden.show();
            coupon_not_hidden.hide();
        } else {
            coupon_hidden.hide();
            coupon_not_hidden.show();
        }
    });
});

// For tabs , colorpicker and choosing of type of shortcode
jQuery(document).ready(function($) {

    /**
     * Function tabs
     * used in tabs of setting page
     * @returns void
     */
    window.tabs = function() {
        //$('form').append($('.tabs .form-table'));
        $($('.wpcd_settings_section .nav-tab-wrapper .form-table').get().reverse()).each(function() {
            $(this).insertAfter('.nav-tab-wrapper');
        });
        var tabs = $('.wpcd_settings_section button.nav-tab'),
            active = $('.wpcd_settings_section .nav-tab.active'),
            index_active = tabs.index(active),
            tabs_contents = $('.wpcd_settings_section .nav-tab-wrapper').siblings('.form-table'),
            active_content = tabs_contents.eq(index_active);
        if (!tabs) {
            retutn;
        }

        /**
         * hide all tabs
         * except the active one
         */
        tabs_contents.each(function() {
            $(this).hide();
        });
        active_content.show();

        /**
         * change the tab content when click
         * by giving the button active class
         */
        tabs.each(function() {
            $(this).click(function(e) {
                // check if the active and the clicked button is not the same
                if ($(this)[0] !== active[0]) {
                    $(this).addClass('active');
                    active.removeClass('active');

                    //call the function to show the active content
                    window.tabs();
                }
            });
        });
    };
    window.tabs();

    /**
     * For color pickers
     */

    var wpcd_colorSelectors = $('.wpcd_colorSelectors');
    if ($.isFunction($(wpcd_colorSelectors[0]).ColorPicker))
        for ($i = 0; $i < wpcd_colorSelectors.length; $i++) {
            $(wpcd_colorSelectors[$i]).ColorPicker({
                onShow: function(colpkr) {
                    $(colpkr).fadeIn(500);
                    return false;
                },
                onHide: function(colpkr) {
                    $(colpkr).fadeOut(500);
                    return false;
                },
                onChange: function(hsb, hex, rgb) {
                    $this = $('#' + this.data('targetid'));

                    $this.children('div').css('backgroundColor', '#' + hex);
                    $this.children('input').val('#' + hex);
                }
            });
        }

    /**
     * for widget category filter
     */
    window.widget;
    var category_filter_select_widget = $('.coupon_category_filter_select_widget');


    category_filter_select_widget.each(function() {
        //you are in widget page
        window.widget = 1;
        
        //hide all coupons
        $(this).parent().next().children('datalist').children('option[category-title]').prop('disabled',true);

        //show the coupony of the selected category
        $(this).parent().next().children('datalist').children('option[category-title="' + $(this).val() + '"]').prop('disabled',false);

        $(this).change(function() {
            $(this).parent().next().children('input').val('');
            //hide all coupons
            $(this).parent().next().children('datalist').children('option[category-title]').prop('disabled',true);

            //show the coupony of the selected category
            $(this).parent().next().children('datalist').children('option[category-title="' + $(this).val() + '"]').prop('disabled',false);
        });
    });




//Feature of choosing Archive , category or Single shortcode
    window.coupons_shortcode_type = $('#coupons_shortcode_type');

//for archive
    window.coupons_style_select = $('#coupons_style_select');
    window.coupons_template_select = $('#coupons_template_select');

//for category
    window.coupons_style_category_select = $('#coupons_style_category_select');
    window.coupons_template_category_select = $('#coupons_template_category_select');

    function WpcdCouponChoosingInsert() {
        function displayNoneforAll() {
            $('.shortcode_inserter_select').not('.wpcd_types_select').hide();
        }
        displayNoneforAll();
        if (coupons_shortcode_type.val() === 'archive') {
            $('.shortcode_inserter_select.wpcd_style_select').show();

            //check if horizontal style chosen
            if ($('#coupons_style_select').val() === 'horizontal')
                $('.shortcode_inserter_select.wpcd_template_select').show();
            else
                $('.shortcode_inserter_select.wpcd_template_select').hide();


            $('#coupons_style_select').change(function() {
                WpcdCouponChoosingInsert();
            });

        } else if (coupons_shortcode_type.val() === 'category') {
            $('.shortcode_inserter_select.wpcd_categories_select').show();
            $('.shortcode_inserter_select.wpcd_style_category_select').show();
            //check if horizontal style chosen
            if ($('#coupons_style_category_select').val() === 'horizontal')
                $('.shortcode_inserter_select.wpcd_template_category_select').show();
            else
                $('.shortcode_inserter_select.wpcd_template_category_select').hide();

            $('#coupons_style_category_select').change(function() {
                WpcdCouponChoosingInsert();
            });

        } else if (coupons_shortcode_type.val() === 'single') {
            if (window.widget === 1)
                return;
            $('.shortcode_inserter_select.wpcd_coupons_select').show();
            $('.shortcode_inserter_select.wpcd_type_select').show();
            //filter Category select
            $('.shortcode_inserter_select.wpcd_category_filter_select').show();

            //The datalist element
            var coupon_select = $('#coupon_list');

            //hide all option that have category
            coupon_select.children('option[category-title]').prop('disabled',true);

            //show the coupony of the selected category
            $('option[category-title="' + $('#select_category_filter').val() + '"]').prop('disabled',false);
            $('#select_category_filter').change(function() {
                $('#coupon_select').val("");
                WpcdCouponChoosingInsert();
            });

        }else { // free version
            $('.shortcode_inserter_select.wpcd_coupons_select').show();
            $('.shortcode_inserter_select.wpcd_type_select').show();
        }

    }

    WpcdCouponChoosingInsert();
    coupons_shortcode_type.change(function() {
        WpcdCouponChoosingInsert();

        //resize the window
        thickbox_resize();
    });

});



/* Premium Code Stripped by Freemius */


//Inserts coupon shortcode.
function WpcdCouponInsertFree() {
    var $coupon_select = jQuery("#coupon_select");
    var coupon_shortcode_type = jQuery("#coupon_shortcode_type");
    var coupon_id = $coupon_select.val();
    if (coupon_shortcode_type.val() === 'coupon') {
        window.send_to_editor("[wpcd_coupon id=" + coupon_id + "]");
    } else if (coupon_shortcode_type.val() === 'code') {
        window.send_to_editor("[wpcd_code id=" + coupon_id + "]");
    }

}

//Update Counter on date Change

function update_two_counter_date(data) {
    //alert(data);
    var coup_date = data;
    if (coup_date.indexOf("-") >= 0) {
        var dateAr = coup_date.split('-');
        coup_date = dateAr[1] + '/' + dateAr[0] + '/' + dateAr[2];
    }
    selectedDate = coup_date + ' ' + jQuery("#expire-time").val();
    $clock.countdown(selectedDate.toString());
}

//Adding the tooltip to show when hovered.
jQuery(document).ready(function($) {
    $(document).ready(function() {
        $('.masterTooltip').hover(function() {
            var title = $(this).attr('title');
            $(this).data('tipText', title).removeAttr('title');
            $('<p class="tooltip"></p>')
                .text(title)
                .appendTo('body')
                .fadeIn('slow');
        }, function() {
            $(this).attr('title', $(this).data('tipText'));
            $('.tooltip').remove();
        }).mousemove(function(e) {
            var mousex = e.pageX + 20;
            var mousey = e.pageY + 10;
            $('.tooltip')
                .css({top: mousey, left: mousex})
        });
    });
});

//Resizes the coupon inserter popup.
function thickbox_resize() {
    jQuery(function($) {
        var $thickbox = $("#TB_window");
        if ($thickbox.find(".wpcd_shortcode_insert").length > 0) {
            var coupon_inserter_height = $('.wpcd_shortcode_insert').outerHeight() + $('.wpcd_shortcode_insert-bt').outerHeight() + $('#TB_title').outerHeight();
            var $ajax_content = $("#TB_ajaxContent");
            $thickbox.height((coupon_inserter_height - 20));
            $ajax_content.height((coupon_inserter_height));
            $ajax_content.css({'width': '100%', 'padding': '0'});
        }
    });
}
jQuery(function($) {
    $('#wpcd_add_shortcode').on('click', function() {
        setTimeout(function() {
            thickbox_resize();
        }, 10);
    });
    $(window).on('resize load', function() {
        thickbox_resize();
    });
});

//Default preview metabox.
jQuery(document).ready(function($) {

    $(function() {

        //change title dynamically
        $('#title').keyup(function() {
            var title = $(this).val();
            $('.wpcd-coupon-title').text(title);
            $('.wpcd-coupon-one-title').text(title);
            $('.wpcd-coupon-three-title').text(title);
        });

        //change description dynamically
        $('#description').keyup(function() {
            var description = $(this).val();
            $('.wpcd-coupon-description').text(description);
        });

        $('#discount-text').keyup(function() {
            var discount_text = $(this).val();
            $('.wpcd-coupon-discount-text').text(discount_text);
            $('.wpcd-coupon-one-discount-text').text(discount_text);
            $('.wpcd-coupon-two-discount-text').text(discount_text);
        });

        $('#coupon-code-text').keyup(function() {
            var coupon_code_text = $(this).val();
            $('.coupon-code-button').text(coupon_code_text);
            $('.wpcd-coupon-one-btn').text(coupon_code_text);
        });

        $('#deal-button-text').keyup(function() {
            var deal_code_text = $(this).val();
            $('.deal-code-button').text(deal_code_text);
            $('.wpcd-coupon-one-btn').text(deal_code_text);
        });

        var coupon_code_div = $('.wpcd-coupon-code');
        var deal_code_div = $('.wpcd-deal-code');
        var coupon_one_coupon = $('.wpcd-coupon-one-coupon');
        var coupon_one_deal = $('.wpcd-coupon-one-deal');
        var coupon_two_coupon = $('.wpcd-coupon-two-coupon-code');
        var coupon_two_deal = $('.wpcd-coupon-two-deal');
        var coupon_three_coupon = $('.wpcd-coupon-three-coupon-code');
        var coupon_three_deal = $('.wpcd-coupon-three-deal');


        if ($('#coupon-type').val() === 'Coupon') {
            coupon_code_div.show();
            coupon_one_coupon.show();
            coupon_two_coupon.show();
            coupon_three_coupon.show();
            deal_code_div.hide();
            coupon_one_deal.hide();
            coupon_two_deal.hide();
            coupon_three_deal.hide();
        } else {
            coupon_code_div.hide();
            coupon_one_coupon.hide();
            coupon_two_coupon.hide();
            coupon_three_coupon.hide();
            deal_code_div.show();
            coupon_one_deal.show();
            coupon_two_deal.show();
            coupon_three_deal.show();
        }

        $('[name="coupon-type"]').on('change', function() {
            if ($(this).val() === 'Coupon') {
                $('.coupon-type').text('Coupon');
                coupon_code_div.show("slow");
                coupon_one_coupon.show();
                deal_code_div.hide("slow");
                coupon_one_deal.hide();
            } else {
                $('.coupon-type').text('Deal');
                coupon_code_div.hide("slow");
                coupon_one_coupon.hide();
                deal_code_div.show("slow");
                coupon_one_deal.show();
            }
        });


        var coupon_expiration = $('.coupon-expiration');
        var coupon_expire = $('.wpcd-coupon-expire');
        var coupon_expired = $('.wpcd-coupon-expired');
        var coupon_one_expire = $('.wpcd-coupon-one-expire');
        var coupon_one_expired = $('.wpcd-coupon-one-expired');
        var coupon_three_expire = $('.wpcd-coupon-three-expire');
        var coupon_three_expired = $('.wpcd-coupon-three-expired');

        $('[name="show-expiration"]').on('change', function() {
            if ($(this).val() === 'Show') {
                coupon_expiration.show();
                var expire_date = $('#expire-date');
                $('[name="expire-date"]').on('select', function() {
                    $('.expiration-date').text(expire_date);
                });
            } else {
                coupon_expiration.hide();
                coupon_expire.hide();
                coupon_expired.hide();
                coupon_one_expire.hide();
                coupon_one_expired.hide();
                coupon_three_expire.hide();
                coupon_three_expired.hide();
            }
        });

    });

});

//Changing templates.
jQuery(document).ready(function($) {
    var couponDefault = $('.wpcd-coupon');
    var couponOne = $('.wpcd-coupon-one');
    var couponTwo = $('.wpcd-coupon-two');
    var couponThree = $('.wpcd-coupon-three');
    var couponTemplate = $('#coupon-template');

    if (couponTemplate.val() === 'Default') {
        couponOne.hide();
        couponTwo.hide();
        couponThree.hide();
    } else if (couponTemplate.val() === 'Template One') {
        couponDefault.hide();
        couponOne.show("slow");
        couponTwo.hide();
        couponThree.hide();
    } else if (couponTemplate.val() === 'Template Two') {
        couponDefault.hide();
        couponOne.hide();
        couponTwo.show("slow");
        couponThree.hide();
    } else if (couponTemplate.val() === 'Template Three') {
        couponDefault.hide();
        couponOne.hide();
        couponTwo.hide();
        couponThree.show("slow");
    }

    $(couponTemplate).on('change', function() {
        if ($(this).val() === 'Default') {
            couponDefault.show("slow");
            couponOne.hide();
            couponTwo.hide();
            couponThree.hide();
        } else if ($(this).val() === 'Template One') {
            couponDefault.hide();
            couponOne.show("slow");
            couponTwo.hide();
            couponThree.hide();
        } else if ($(this).val() === 'Template Two') {
            couponDefault.hide();
            couponOne.hide();
            couponTwo.show("slow");
            couponThree.hide();
        } else if ($(this).val() === 'Template Three') {
            couponDefault.hide();
            couponOne.hide();
            couponTwo.hide();
            couponThree.show("slow");
        }
    });

});

function wpcd_featured_img_func() {
    var imgSrc = jQuery("#set-post-thumbnail img").attr("src");
    var imgDef = jQuery(".wpcd-default-img").attr("default-img");
    if (typeof imgDef !== "undefined") {
        if (typeof imgSrc !== "undefined") {
            jQuery(".wpcd-get-fetured-img").attr("src", imgSrc);
        } else {
            jQuery(".wpcd-get-fetured-img").attr("src", imgDef);
        }
    }
}