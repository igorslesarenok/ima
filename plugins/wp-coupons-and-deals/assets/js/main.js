//for count down
jQuery(document).ready(function($) {
    var count_down_span = $('[data-countdown_coupon]');

    count_down_span.each(function() {

        var $this = $(this), finalDate = $(this).data('countdown_coupon');
        $this.countdown(finalDate, function(event) {
            var format = '%M ' + wpcd_main_js.minutes + ' %S ' + wpcd_main_js.seconds;
            if (event.offset.hours > 0) {
                format = '%H ' + wpcd_main_js.hours + ' %M ' + wpcd_main_js.minutes + ' %S ' + wpcd_main_js.seconds;
            }
            if (event.offset.totalDays > 0) {
                format = '%-d ' + wpcd_main_js.day + '%!d ' + format;
            }
            if (event.offset.weeks > 0) {
                format = '%-w ' + wpcd_main_js.week + '%!w ' + format;
            }
            if (event.offset.weeks == 0 && event.offset.totalDays == 0 && event.offset.hours == 0 && event.offset.minutes == 0 && event.offset.seconds == 0) {
                jQuery(this).parent().addClass('wpcd-countdown-expired').html( wpcd_main_js.expired_text );
            } else {
                jQuery(this).html(event.strftime(format));
            }

        })

        .on('finish.countdown', function(event) {
             jQuery('.wpcd-coupon-two-countdown-text').hide();
             jQuery(this).html( wpcd_main_js.expired_text ).parent().addClass('disabled');
        });

    });
});

jQuery(document).ready(function($){
    var num_words        = Number(wpcd_main_js.word_count);
    var full_description = $('.wpcd-full-description');
    var more             = $('.wpcd-more-description');
    var less             = $('.wpcd-less-description');
    full_description.each(function(){
        $this            = $(this);

        var full_content = $this.html();
        var check       = full_content.split(' ').length > num_words;
        if(check){
            var short_content= full_content.split(' ').slice(0,num_words).join(' ');
            $this.siblings('.wpcd-short-description').html(short_content+'...');
            $this.hide();
            $this.siblings('.wpcd-less-description').hide();
        }else{
            $(this).siblings('.wpcd-more-description').hide();
            $(this).siblings('.wpcd-more-description');
            $(this).siblings('.wpcd-less-description').hide();
            $(this).siblings('.wpcd-short-description').hide();
        }
    });
    // more and less link
    more.click(function(e){
        e.preventDefault();
        $(this).siblings('.wpcd-full-description').show();
        $(this).siblings('.wpcd-less-description').show();
        $(this).siblings('.wpcd-short-description').hide();
        $(this).hide();

    });
    less.click(function(e){
        e.preventDefault();
        $(this).siblings('.wpcd-short-description').show();
        $(this).siblings('.wpcd-more-description').show();
        $(this).siblings('.wpcd-full-description').hide();
        $(this).hide();
    });
});

jQuery(document).ready(function($) {
    $(document).ready(function() {
        $('.masterTooltip').hover(function() {
            var title = $(this).attr('title');
            $(this).data('tipText', title).removeAttr('title');
            $('<p class="tooltip"></p>')
                    .text(title)
                    .appendTo('body')
                    .fadeIn('slow');
        }, function() {
            $(this).attr('title', $(this).data('tipText'));
            $('.tooltip').remove();
        }).mousemove(function(e) {
            var mousex = e.pageX + 20;
            var mousey = e.pageY + 10;
            $('.tooltip')
                    .css({top: mousey, left: mousex})
        });
    });

    
/* Premium Code Stripped by Freemius */


    
/* Premium Code Stripped by Freemius */


});

function wpcdCopyToClipboard(element) {
    var $temp = jQuery("<input>");
    
    jQuery("body").append($temp);
    $temp.val(jQuery(jQuery(element)[0]).text()).select();
    document.execCommand("copy");
    $temp.remove();
}

function wpcdOpenCouponAffLink(CoupenId) {
    var a    = jQuery("#coupon-button-" + CoupenId);
    var theLink = a.attr("data-aff-url");
    window.open(a.attr('href'),'_blank');
    window.location = theLink;
    return false;
}
