<?php

// If accessed directly, exit
if ( !defined( 'ABSPATH' ) ) {
    die;
}
/**
 * This class builds the short code that
 * will be used to display coupon codes
 * on the front end.
 *
 * @since 1.0
 * @author Imtiaz Rayhan
 */
class WPCD_Short_Code
{
    /**
     * Class constructor.
     * Adds the function to register the shortcode with WordPress.
     *
     * @since 1.0
     */
    public static function init()
    {
        /**
         * Shortcode register function.
         *
         * @since 1.0
         */
        add_shortcode( 'wpcd_coupon', array( __CLASS__, 'wpcd_coupon' ) );
        add_shortcode( 'wpcd_code', array( __CLASS__, 'wpcd_coupon_code' ) );
        
        if ( wcad_fs()->is_plan__premium_only( 'pro' ) or wcad_fs()->is_trial() ) {
            add_shortcode( 'wpcd_coupons', array( __CLASS__, 'wpcd_coupons_archive_func__premium_only' ) );
            add_shortcode( 'wpcd_coupons_loop', array( __CLASS__, 'wpcd_coupons_loop_func__premium_only' ) );
        }
    
    }
    
    /**
     * Shortcode attributes and arguments to build the shortcode.
     *
     * @param $atts array shortcode attributes.
     * @return string
     *
     * @since 1.0
     */
    public static function wpcd_coupon( $atts )
    {
        global  $wpcd_atts ;
        global  $wpcd_coupon ;
        /**
         * These are the shortcode attributes.
         *
         * @since 1.0
         */
        $wpcd_atts = shortcode_atts( array(
            'id'    => '',
            'total' => '-1',
        ), $atts, 'wpcd_coupon' );
        /**
         * Arguments to be used for a custom Query.
         *
         * @since 1.0
         */
        $wpcd_arg = array(
            'p'              => esc_attr( $wpcd_atts['id'] ),
            'posts_per_page' => esc_attr( $wpcd_atts['total'] ),
            'post_type'      => 'wpcd_coupons',
            'post_status'    => 'publish',
        );
        /**
         * New custom query to get post and post data
         * from the custom coupon post type.
         *
         * @since 1.0
         */
        $wpcd_coupon = new WP_Query( $wpcd_arg );
        $output = '';
        while ( $wpcd_coupon->have_posts() ) {
            $wpcd_coupon->the_post();
            global  $coupon_id ;
            $template = new WPCD_Template_Loader();
            $coupon_id = get_the_ID();
            $coupon_template = get_post_meta( $coupon_id, 'coupon_details_coupon-template', true );
            
            if ( wcad_fs()->is_plan__premium_only( 'pro' ) or wcad_fs()->is_trial() ) {
                
                if ( $coupon_template == 'Template One' ) {
                    ob_start();
                    $template->get_template_part( 'shortcode-one__premium_only' );
                    $output = ob_get_clean();
                } elseif ( $coupon_template == 'Template Two' ) {
                    ob_start();
                    $template->get_template_part( 'shortcode-two__premium_only' );
                    $output = ob_get_clean();
                } elseif ( $coupon_template == 'Template Three' ) {
                    ob_start();
                    $template->get_template_part( 'shortcode-three__premium_only' );
                    // Return Variables
                    $output = ob_get_clean();
                } else {
                    ob_start();
                    $template->get_template_part( 'shortcode-default' );
                    $output = ob_get_clean();
                }
            
            } else {
                ob_start();
                $template->get_template_part( 'shortcode-default' );
                $output = ob_get_clean();
            }
        
        }
        wp_reset_postdata();
        return $output;
    }
    
    /**
     * Builds the only coupon code shortcode.
     *
     * @param $atts
     * @return string
     *
     * @since 1.0
     */
    public static function wpcd_coupon_code( $atts )
    {
        global  $wpcd_code_atts ;
        global  $wpcd_coupon_code ;
        /**
         * These are the shortcode attributes.
         *
         * @since 1.4
         */
        $wpcd_code_atts = shortcode_atts( array(
            'id'    => '',
            'total' => '-1',
        ), $atts, 'wpcd_code' );
        /**
         * Arguments to be used for a custom Query.
         *
         * @since 1.4
         */
        $wpcd_code_arg = array(
            'p'              => esc_attr( $wpcd_code_atts['id'] ),
            'posts_per_page' => esc_attr( $wpcd_code_atts['total'] ),
            'post_type'      => 'wpcd_coupons',
            'post_status'    => 'publish',
        );
        /**
         * New custom query to get post and post data
         * from the custom coupon post type.
         *
         * @since 1.4
         */
        $wpcd_coupon_code = new WP_Query( $wpcd_code_arg );
        $template = new WPCD_Template_Loader();
        ob_start();
        $template->get_template_part( 'shortcode-code' );
        // Return Variables
        return ob_get_clean();
    }

}