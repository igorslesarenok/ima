<?php

// Exit if accessed directly.
if ( !defined( 'ABSPATH' ) )
	exit;

/**
 * This class constructs the meta box for coupon add page.
 *
 * @since 1.0
 */

class WPCD_Meta_Boxes {

	/**
	 * Post types on which metabox will be added.
	 *
	 * @var array post types.
	 * @since 1.0
	 */
	private $post_types = array(
		'wpcd_coupons',
	);

	/**
	 * Setting up the fields for the metabox.
	 *
	 * @var array Fields.
	 * @since 1.0
	 */
	private $wpcd_fields = array();

	/**
	 * Class construct method.
	 * Adding actions to WordPress hooks.
	 *
	 * @since 1.0
	 */
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_post' ) );

		$this->wpcd_fields = array(
			array(
				'id' => 'coupon-type',
				'label' => __( 'Coupon Type', 'wpcd-coupon'),
				'type' => 'select',
				'help' => __( 'Coupon Type. Coupon will display a coupon code which will be copied when user clicks on it. Deal will display a link to get the deal instead of coupon code.', 'wpcd-coupon' ),
				'options' => array(
					'Coupon',
					'Deal',
				),
			),
			array(
				'id' => 'coupon-code-text',
				'label' => __( 'Coupon Code', 'wpcd-coupon' ),
				'type' => 'buttontext',
				'help' => __( 'Put your coupon code here. This will be copied when user clicks on it.', 'wpcd-coupon' )
			),
			array(
				'id' => 'deal-button-text',
				'label' => __( 'Deal Button Text', 'wpcd-coupon'),
				'type' => 'dealtext',
				'help' => __( 'Deal button text. Put something like Get this Deal.', 'wpcd-coupon' )
			),
			array(
				'id' => 'link',
				'label' => __( 'Link', 'wpcd-coupon' ),
				'type' => 'text',
				'help' => __( 'Link to be opened when clicked on coupon code. You can use your affiliate links.', 'wpcd-coupon' )
			),
			array(
				'id' => 'discount-text',
				'label' => __( 'Discount Amount/Text', 'wpcd-coupon' ),
				'type' => 'text',
				'help' => __( 'Discount amount or text to be shown. Example: 60% Off.', 'wpcd-coupon' )
			),
			array(
				'id' => 'description',
				'label' => __( 'Description', 'wpcd-coupon' ),
				'type' => 'textarea',
				'help' => __( 'A little description so users know what the coupon code or deal is about.', 'wpcd-coupon' )
			),
			array(
				'id' => 'show-expiration',
				'label' => __( 'Coupon/Deal Expiration', 'wpcd-coupon' ),
				'type' => 'select',
				'help' => __( 'Choose whether you want to show coupon/deal expiration.', 'wpcd-coupon' ),
				'options' => array(
					'Hide',
					'Show'
				)
			),
			array(
				'id' => 'expire-date',
				'label' => __( 'Expiration Date', 'wpcd-coupon' ),
				'type' => 'expiredate',
				'help' => __( 'Choose a date this coupon will expire. If you leave this blank, shortcode will show the message Doesn\'t expire.', 'wpcd-coupon' )
			),
			array(
				'id' => 'expire-time',
				'label' => __( 'Expiration Time', 'wpcd-coupon' ),
				'type' => 'expiretime',
				'help' => __( 'Choose expiration time of the coupon.', 'wpcd-coupon' )
			),
			array(
				'id' => 'hide-coupon',
				'label' => __('Hide Coupon (Pro)', 'wpcd-coupon'),
				'type' => 'select',
				'help' => __('Choose whether you want to hide the coupun', 'wpcd-coupon'),
				'options' => array(
					'No',
					'Yes'
				)
			),
			array(
				'id' => 'coupon-template',
				'label' => __('Template (Pro)', 'wpcd-coupon'),
				'type' => 'select',
				'help' => __('Choose coupon shortcode template.', 'wpcd-coupon'),
				'options' => array(
					'Default',
					'Template One',
					'Template Two',
					'Template Three'
				)
			)

		);
	}

	/**
	 * Hooks into WordPress' add_meta_boxes function.
	 *
	 * @since 1.0
	 */
	public function add_meta_boxes() {
		foreach ( $this->post_types as $post_type ) {
			add_meta_box(
				'coupon-details',
				__( 'Coupon Details', 'wpcd-coupon' ),
				array( $this, 'add_meta_box_callback' ),
				$post_type,
				'normal',
				'core'
			);
		}
	}

	/**
	 * Generating the HTML for the meta box
	 *
	 * @param object $post WordPress post object
	 * @since 1.0
	 */
	public function add_meta_box_callback( $post ) {
		wp_nonce_field( 'coupon_details_data', 'coupon_details_nonce' );
		$this->generate_wpcd_fields( $post );
	}

	/**
	 * Generating the field's HTML for the meta box.
	 *
	 * @since 1.0
	 */
	public function generate_wpcd_fields( $post ) {
		$expireDateFormat = get_option( 'wpcd_expiry-date-format' );
		$output = '';
		$help = '';

		foreach ( $this->wpcd_fields as $wpcd_field ) {
			$type = $wpcd_field['type'];
			$label = '<label for="' . $wpcd_field['id'] . '">' . $wpcd_field['label'] . '</label>';
			$db_value = get_post_meta( $post->ID, 'coupon_details_' . $wpcd_field['id'], true );
			switch ( $wpcd_field['type'] ) {

				case 'dealtext':
					$input = sprintf(
						'<input type="text" name="%s" id="%s" value="%s"/><br><i style="font-size: 12px">%s</i>',
						$wpcd_field['id'],
						$wpcd_field['id'],
						$db_value,
						$wpcd_field['help']

					);
					break;

				case 'buttontext':
					$input = sprintf(
						'<input type="text" name="%s" id="%s" value="%s"/><br><i style="font-size: 12px">%s</i>',
						$wpcd_field['id'],
						$wpcd_field['id'],
						$db_value,
						$wpcd_field['help']
					);
					break;

				case 'date':
					$input = sprintf(
						'<input type="%s" name="%s" id="%s" value="%s" /><br><i style="font-size: 12px">%s</i>',
						$wpcd_field['type'],
						$wpcd_field['id'],
						$wpcd_field['id'],
						$db_value,
						$wpcd_field['help']
					);
					break;

				case 'select':
					$input = sprintf(
						'<select id="%s" name="%s"><br><i style="font-size: 12px">%s</i>',
						$wpcd_field['id'],
						$wpcd_field['id'],
						$wpcd_field['help']
					);
					foreach ( $wpcd_field['options'] as $key => $value ) {
						$field_value = !is_numeric( $key ) ? $key : $value;
						$input .= sprintf(
							'<option %s value="%s">%s</option>',
							$db_value === $field_value ? 'selected' : '',
							$field_value,
							$value
						);
					}
					$input .= '</select>';
					break;

				case 'textarea':
					$input = sprintf(
						'<textarea class="large-text" id="%s" name="%s" rows="5">%s</textarea><br><i style="font-size: 12px">%s</i>',
						$wpcd_field['id'],
						$wpcd_field['id'],
						$db_value,
						$wpcd_field['help']
					);
					break;

				case 'expirationcheck':
					$input = sprintf(
						'<input type="checkbox" name="%s" id="%s" value="%s"/><br><i style="font-size: 12px">%s</i>',
						$wpcd_field['id'],
						$wpcd_field['id'],
						$db_value,
						$wpcd_field['help']
					);
					break;

				case 'expiredate':
					$input = sprintf(
						'<input type="text" name="%s" id="%s" placeholder="'.$expireDateFormat.'" value="%s"/><br><i style="font-size: 12px">%s</i>',
						$wpcd_field['id'],
						$wpcd_field['id'],
						$db_value,
						$wpcd_field['help']
					);
					break;

				case 'expiretime':
					$input = sprintf(
						'<input type="text" name="%s" id="%s" value="%s"/><br><i style="font-size: 12px">%s</i>',
						$wpcd_field['id'],
						$wpcd_field['id'],
						$db_value,
						$wpcd_field['help']
					);
					break;



				default:
					$input = sprintf(
						'<input %s id="%s" name="%s" type="%s" value="%s"><br><i style="font-size: 12px">%s</i>',
						$wpcd_field['type'] !== 'color' ? 'class="regular-text"' : '',
						$wpcd_field['id'],
						$wpcd_field['id'],
						$wpcd_field['type'],
						$db_value,
						$wpcd_field['help']
					);
			}
			$output .= $this->row_format( $type, $label, $input );
		}
		echo '<table class="form-table"><tbody>' . $output . $help . '</tbody></table>';
		echo "<script>
				jQuery('#expire-time').timepicker({
					controlType: 'select',
					oneLine: true,
					timeFormat: 'h:m tt',
					showButtonPanel: false
				});
			 </script>";

		if ( wcad_fs()->is_not_paying() ) {
			echo '<p style="font-size: 16px;">' . __('Hide coupon, change templates and get many more features', 'wpcd-coupon') . '- ';

			echo '<a href="' . wcad_fs()->get_upgrade_url() . '">' .
				__('Upgrade to Pro!', 'wpcd-coupon') .
				'</a>';
			echo ' or ';
			echo '<a href="' . wcad_fs()->get_account_url() . '">' .
				__('Start 7 day Free Trial!', 'wpcd-coupon') .
				'</a>' . __('(NO Credit Card Required)', 'wpcd-coupon');
			echo '. ';
			echo '<a href="https://wpcouponsdeals.com" target="_blank">' . __('More information about the Pro verison.', 'wpcd-coupon') . '</a>';
		}

		/**
		 * Custom date format for the coupons and deals.
		 *
		 * @since 1.3
		 */
		if ( $expireDateFormat === 'mm/dd/yy' ) {

			echo "<script>
                jQuery('#expire-date').datepicker(  {
                    dateFormat : 'mm/dd/yy',
                    showOtherMonths: true,
                    onSelect: function(dateText) {
                      jQuery('.expiration-date').text(dateText);
                      update_two_counter_date(dateText);
                    }
               	});
                
			</script>";

		} else if ( $expireDateFormat === 'yy/mm/dd' ) {

			echo "<script>
                jQuery('#expire-date').datepicker(  {
                    dateFormat : 'yy/mm/dd',
                    showOtherMonths: true,
                    onSelect: function(dateText) {
                      jQuery('.expiration-date').text(dateText);
                      update_two_counter_date(dateText);
                    }
                });
			</script>";

		} else {

			echo "<script>
                jQuery('#expire-date').datepicker(  {
                    dateFormat : 'dd-mm-yy',
                    showOtherMonths: true,
                    onSelect: function(dateText) {
                      jQuery('.expiration-date').text(dateText);
                      update_two_counter_date(dateText);
                    }
                });
			</script>";

		}

	}

	/**
	 * Generates the HTML for table rows.
	 *
	 * @since 1.0
	 */
	public function row_format( $type, $label, $input ) {
		return sprintf(
			'<tr id="%s"><th scope="row">%s</th><td>%s</td></tr>',
			$type,
			$label,
			$input
		);
	}
	/**
	 * Hooks into WordPress' save_post function.
	 *
	 * @since 1.0
	 */
	public function save_post( $post_id ) {
		if ( ! isset( $_POST['coupon_details_nonce'] ) )
			return $post_id;

		$nonce = $_POST['coupon_details_nonce'];
		if ( !wp_verify_nonce( $nonce, 'coupon_details_data' ) )
			return $post_id;

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return $post_id;

		foreach ( $this->wpcd_fields as $wpcd_field ) {
			if ( isset( $_POST[ $wpcd_field['id'] ] ) ) {
				switch ( $wpcd_field['type'] ) {
					case 'email':
						$_POST[ $wpcd_field['id'] ] = sanitize_email( $_POST[ $wpcd_field['id'] ] );
						break;
					case 'text':
						$_POST[ $wpcd_field['id'] ] = sanitize_text_field( $_POST[ $wpcd_field['id'] ] );
						break;
				}

				$field_checker = 'coupon_details_' . $wpcd_field['id'];

				if ( $field_checker == 'coupon_details_hide-coupon' ) {
					update_post_meta( $post_id, 'coupon_details_' . $wpcd_field['id'], 'No' );
				} elseif ( $field_checker == 'coupon_details_coupon-template' ) {
					update_post_meta( $post_id, 'coupon_details_' . $wpcd_field['id'], 'Default' );
				} else {
					update_post_meta( $post_id, 'coupon_details_' . $wpcd_field['id'], $_POST[ $wpcd_field['id'] ] );
				}

			} else if ( $wpcd_field['type'] === 'checkbox' ) {
				update_post_meta( $post_id, 'coupon_details_' . $wpcd_field['id'], '0' );
			}
		}
	}
}