<?php

/**
 * Builds the coupon preview meta box.
 *
 * @since 2.0
 */

$post_id = get_the_ID();
$title = get_the_title();
$description = get_post_meta( $post_id, 'coupon_details_description', true );
$coupon_thumbnail = get_the_post_thumbnail_url( $post_id );
$coupon_type = get_post_meta( $post_id, 'coupon_details_coupon-type', true );
$discount_text = get_post_meta( $post_id, 'coupon_details_discount-text', true );
$link = get_post_meta( $post_id, 'coupon_details_link', true );
$coupon_code = get_post_meta( $post_id, 'coupon_details_coupon-code-text', true );
$deal_text = get_post_meta( $post_id, 'coupon_details_deal-button-text', true );
$coupon_hover_text = get_option( 'wpcd_coupon-hover-text' );
$deal_hover_text = get_option( 'wpcd_deal-hover-text' );
$button_class = '.wpcd-btn-' . $post_id;
$no_expiry = get_option( 'wpcd_no-expiry-message' );
$expire_text = get_option( 'wpcd_expire-text' );
$expired_text = get_option( 'wpcd_expired-text' );
$hide_coupon_text = get_option( 'wpcd_hidden-coupon-text' );
$hide_coupon_button_color = get_option('wpcd_hidden-coupon-button-color');
$hidden_coupon_hover_text = get_option( 'wpcd_hidden-coupon-hover-text' );
$show_expiration = get_post_meta( $post_id, 'coupon_details_show-expiration', true );
$today = date( 'd-m-Y' );
$time_now = time();
$expire_date = get_post_meta( $post_id, 'coupon_details_expire-date', true );
$expire_time = get_post_meta( $post_id, 'coupon_details_expire-time', true );
$expireDateFormat = get_option( 'wpcd_expiry-date-format' );
$expire_date_format = date("m/d/Y", strtotime($expire_date));
$hide_coupon = get_post_meta( $post_id, 'coupon_details_hide-coupon', true );

$wpcd_text_to_show = get_option('wpcd_text-to-show');
$wpcd_custom_text  = get_option('wpcd_custom-text');

if ( $wpcd_text_to_show == 'description' ) {
    $wpcd_custom_text = $description;
} else {
    if ( empty($wpcd_custom_text)) {
	    $wpcd_custom_text = __("Click on 'Copy' to Copy the Coupon Code.", 'wpcd-coupon');
    }
}

?>

<style>
    .wpcd-coupon-button-type .coupon-code-wpcd .get-code-wpcd{
        background-color:<?php echo $hide_coupon_button_color; ?>;
    }.wpcd-coupon-button-type .coupon-code-wpcd .get-code-wpcd:after{
         border-left-color:<?php echo $hide_coupon_button_color; ?>;
     }
</style>
<span class="wpcd-default-img" default-img="<?php echo WPCD_Plugin::instance()->plugin_assets . 'img/icon-128x128.png'; ?>" style="display:none;"></span>
<div class="wpcd-coupon wpcd-coupon-default wpcd-coupon-id-<?php echo $coupon_id; ?>">
	<div class="wpcd-col-1-8">
		<div class="wpcd-coupon-discount-text">
			<?php if ( !empty( $discount_text ) ) { echo $discount_text; } else { echo __('Discount Text', 'wpcd-coupon'); } ?>
        </div>
		<div class="coupon-type">
			<?php if ( !empty( $coupon_type ) ) { echo $coupon_type; } else { echo __('Coupon', 'wpcd-coupon'); } ?>
        </div>
	</div>
	<div class="wpcd-coupon-content wpcd-col-7-8">
		<div class="wpcd-coupon-header">
			<div class = "wpcd-col-3-4">
				<div class = "wpcd-coupon-title"><?php if ( !empty( $title ) ) { echo $title; } else { echo __('Sample Coupon Code', 'wpcd-coupon'); } ?>
					</div>
			</div>
			<div class="wpcd-col-1-4">
                <div class="coupon-code-wpcd coupon-detail wpcd-coupon-button-type wpcd-coupon-hidden">
                    <a data-type="code" data-coupon-id="<?php echo $post_id; ?>" href="" class="coupon-button coupon-code-wpcd masterTooltip" id="coupon-button-<?php echo $post_id; ?>" title="<?php if (!empty($hidden_coupon_hover_text)) { echo $hidden_coupon_hover_text; } else { _e('Click Here to Show Code', 'wpcd-coupon'); } ?>" data-position="top center" data-inverted="" data-aff-url="<?php echo $link; ?>" >
                        <span class="code-text-wpcd" rel="nofollow"><?php if ( !empty( $coupon_code ) ) { echo $coupon_code; } else { echo __('COUPONCODE', 'wpcd-coupon'); } ?></span>
                        <span class="get-code-wpcd">
                            <?php
	                        if ( !empty( $hide_coupon_text ) ) {
		                        echo $hide_coupon_text;
	                        }  else {
		                        echo __('Show Code', 'wpcd-coupon');
	                        }
	                        ?>
                        </span>
                    </a>
                </div>
                <div class="wpcd-coupon-not-hidden">
                    <div class="wpcd-coupon-code">
                        <button class="wpcd-btn masterTooltip wpcd-coupon-button" title="<?php echo __('Click Here To Copy Coupon', 'wpcd-coupon'); ?>" data-clipboard-text="<?php if ( !empty( $coupon_code ) ) { echo $coupon_code; } else { echo __('COUPONCODE', 'wpcd-coupon'); } ?>">
                            <span class="wpcd_coupon_icon"></span> <span class="coupon-code-button"><?php if ( !empty( $coupon_code ) ) { echo $coupon_code; } else { echo __('COUPONCODE', 'wpcd-coupon'); } ?></span>
                        </button>
                    </div>
                    <div class="wpcd-deal-code">
                        <button class="wpcd-btn masterTooltip wpcd-deal-button" title="<?php echo __('Click Here To Get this deal', 'wpcd-coupon'); ?>" data-clipboard-text="<?php if ( !empty( $deal_text ) ) { echo $deal_text; } else { echo __('Claim This Deal', 'wpcd-coupon'); } ?>">
                            <span class="wpcd_deal_icon"></span><span class="deal-code-button"><?php if ( !empty( $deal_text ) ) { echo $deal_text; } else { echo __('Claim This Deal', 'wpcd-coupon'); } ?></span>
                        </button>
                    </div>
                </div>
			</div>

		</div>

		<div class ="wpcd-extra-content">
			<div class="wpcd-col-3-4">
				<div class="wpcd-coupon-description">
					<?php if ( !empty( $description ) ) { echo $description; } else { echo __('This is the description of the coupon code. Additional details of what the coupon or deal is.', 'wpcd-coupon'); } ?>
                </div>
			</div>

			<div class="wpcd-col-1-4">
                <div class="coupon-expiration wpcd-coupon-expire">
                    <span>
                        <?php
	                    if ( !empty( $expire_text ) ) {
		                    echo $expire_text;
	                    } else {
		                    echo __('Expires on: ', 'wpcd-coupon');
	                    }
	                    ?>
                    </span><span class="expiration-date"></span>
                </div>
				<?php
				if ( $coupon_type == 'Coupon' ) {
					if ( $show_expiration == 'Show' ) {
						if ( !empty( $expire_date ) ) {
							if ( strtotime( $expire_date ) >= strtotime( $today ) ) { ?>
                                <div class="wpcd-coupon-expire">
	                                <?php
	                                if ( !empty( $expire_text ) ) {
		                                echo $expire_text . ' ' . '<span class="expiration-date">' . $expire_date . '</span>'; ;
	                                } else {
		                                echo __('Expires on: ', 'wpcd-coupon') . '<span class="expiration-date">' . $expire_date . '</span>';
	                                }
	                                ?>
                                </div>
							<?php } elseif ( strtotime( $expire_date ) < strtotime( $today ) ) { ?>
                                <div class="wpcd-coupon-expired">
	                                <?php
	                                if ( !empty( $expired_text ) ) {
		                                echo $expired_text . ' ' . '<span class="expiration-date">' . $expire_date . '</span>'; ;
	                                } else {
		                                echo __('Expired on: ', 'wpcd-coupon') . '<span class="expiration-date">' . $expire_date . '</span>';
	                                }
	                                ?>
                                </div>
							<?php }
						} else { ?>
                            <div class="wpcd-coupon-expire">
								<?php if ( !empty( $no_expiry ) ) {
									echo $no_expiry;
								} else {
									echo __("Doesn't expire", 'wpcd-coupon');
								} ?>
                            </div>
						<?php }
					} else {
						echo '';
					}

				} elseif ( $coupon_type == 'Deal' ) {
					if ( $show_expiration == 'Show' ) {
						if ( !empty( $expire_date ) ) {
							if ( strtotime( $expire_date ) >= strtotime( $today ) ) { ?>
                                <div class="wpcd-coupon-expire">
	                                <?php
	                                if ( !empty( $expire_text ) ) {
		                                echo $expire_text . ' ' . '<span class="expiration-date">' . $expire_date . '</span>'; ;
	                                } else {
		                                echo __('Expires on: ', 'wpcd-coupon') . '<span class="expiration-date">' . $expire_date . '</span>';
	                                }
	                                ?>
                                </div>
							<?php } elseif ( strtotime( $expire_date ) < strtotime( $today ) ) { ?>
                                <div class="wpcd-coupon-expired">
                                    <div class="wpcd-coupon-expired">
		                                <?php
		                                if ( !empty( $expired_text ) ) {
			                                echo $expired_text . ' ' . '<span class="expiration-date">' . $expire_date . '</span>'; ;
		                                } else {
			                                echo __('Expired on: ', 'wpcd-coupon') . '<span class="expiration-date">' . $expire_date . '</span>';
		                                }
		                                ?>
                                    </div>
                                </div>
							<?php }

						} else { ?>

                            <div class="wpcd-coupon-expire">

								<?php	if ( !empty( $no_expiry ) ) {
									echo $no_expiry;
								} else {
									echo __("Doesn't expire", 'wpcd-coupon');
								}
								?>
                            </div>

						<?php }
					} else {
						echo '';
					}
				} ?>
			</div>
		</div>
	</div>
</div>

<div class="wpcd-coupon-one">
    <div class="wpcd-col-one-1-8">
		<?php if ( has_post_thumbnail() ) { ?>
            <figure>
                <img class="wpcd-coupon-one-img wpcd-get-fetured-img" src="<?php echo $coupon_thumbnail; ?>">
            </figure>
		<?php } else { ?>
            <figure>
                <img class="wpcd-coupon-one-img wpcd-get-fetured-img" src="<?php echo WPCD_Plugin::instance()->plugin_assets . 'img/icon-128x128.png'; ?>">
            </figure>
		<?php } ?>
    </div>
    <div class="wpcd-col-one-7-8">
        <h4 class="wpcd-coupon-one-title"><?php if ( !empty( $title ) ) { echo $title; } else { echo __('Sample Coupon Code', 'wpcd-coupon'); } ?></h4>
        <div id="clear"></div>
        <div class="wpcd-coupon-description">
	        <?php if ( !empty( $description ) ) { echo $description; } else { echo __('This is the description of the coupon code. Additional details of what the coupon or deal is.', 'wpcd-coupon'); } ?>
        </div>
    </div>
    <div class="wpcd-col-one-1-4">
        <div class="wpcd-coupon-one-discount-text">
	        <?php if ( !empty( $discount_text ) ) { echo $discount_text; } else { echo __('Discount Text', 'wpcd-coupon'); } ?>
        </div>
        <div class="coupon-code-wpcd coupon-detail wpcd-coupon-button-type wpcd-coupon-hidden">
            <a data-type="code" data-coupon-id="<?php echo $post_id; ?>" href="" class="coupon-button coupon-code-wpcd masterTooltip" id="coupon-button-<?php echo $post_id; ?>" title="<?php if (!empty($hidden_coupon_hover_text)) { echo $hidden_coupon_hover_text; } else { _e('Click Here to Show Code', 'wpcd-coupon'); } ?>" data-position="top center" data-inverted="" data-aff-url="<?php echo $link; ?>" >
                <span class="code-text-wpcd" rel="nofollow"><?php if ( !empty( $coupon_code ) ) { echo $coupon_code; } else { echo __('COUPONCODE', 'wpcd-coupon'); } ?></span>
                <span class="get-code-wpcd">
                    <?php
	                if ( !empty( $hide_coupon_text ) ) {
		                echo $hide_coupon_text;
	                }  else {
		                echo __('Show Code', 'wpcd-coupon');
	                }
	                ?>
                </span>
            </a>
        </div>
        <div class="wpcd-coupon-not-hidden">
            <div class="wpcd-coupon-code">
                <button class="wpcd-btn masterTooltip wpcd-coupon-button" title="<?php echo __('Click Here To Copy Coupon', 'wpcd-coupon'); ?>" data-clipboard-text="<?php if ( !empty( $coupon_code ) ) { echo $coupon_code; } else { echo __('COUPONCODE', 'wpcd-coupon'); } ?>">
                    <span class="wpcd_coupon_icon"></span> <span class="coupon-code-button"><?php if ( !empty( $coupon_code ) ) { echo $coupon_code; } else { echo __('COUPONCODE', 'wpcd-coupon'); } ?></span>
                </button>
            </div>
            <div class="wpcd-deal-code">
                <button class="wpcd-btn masterTooltip wpcd-deal-button" title="<?php __('Click Here To Get this deal', 'wpcd-coupon'); ?>" data-clipboard-text="<?php if ( !empty( $deal_text ) ) { echo $deal_text; } else { echo __('Claim This Deal', 'wpcd-coupon'); } ?>">
                    <span class="wpcd_deal_icon"></span><span class="deal-code-button"><?php if ( !empty( $deal_text ) ) { echo $deal_text; } else { echo __('Claim This Deal', 'wpcd-coupon'); } ?></span>
                </button>
            </div>
        </div>
        <div class="coupon-expiration wpcd-coupon-one-expire">
            <span>
                <?php
	            if ( !empty( $expire_text ) ) {
		            echo $expire_text;
	            } else {
		            echo __('Expires on: ', 'wpcd-coupon');
	            }
	            ?>
            </span>
            <span class="expiration-date"></span>
        </div>
		<?php
		if ( $coupon_type == 'Coupon' ) {
			if ( $show_expiration == 'Show' ) {
				if ( !empty( $expire_date ) ) {
					if ( strtotime( $expire_date ) >= strtotime( $today ) ) { ?>
                        <div class="wpcd-coupon-one-expire">
	                        <?php
	                        if ( !empty( $expire_text ) ) {
		                        echo $expire_text . ' ' . '<span class="expiration-date">' . $expire_date . '</span>'; ;
	                        } else {
		                        echo __('Expires on: ', 'wpcd-coupon') . '<span class="expiration-date">' . $expire_date . '</span>';
	                        }
	                        ?>
                        </div>
					<?php } elseif ( strtotime( $expire_date ) < strtotime( $today ) ) { ?>
                        <div class="wpcd-coupon-one-expired">
	                        <?php
	                        if ( !empty( $expired_text ) ) {
		                        echo $expired_text . ' ' . '<span class="expiration-date">' . $expire_date . '</span>'; ;
	                        } else {
		                        echo __('Expired on: ', 'wpcd-coupon') . '<span class="expiration-date">' . $expire_date . '</span>';
	                        }
	                        ?>
                        </div>
					<?php }
				} else { ?>
                    <div class="wpcd-coupon-one-expire">
						<?php	if ( !empty( $no_expiry ) ) {
							echo $no_expiry;
						} else {
							echo __("Doesn't expire", 'wpcd-coupon');
						} ?>
                    </div>
				<?php }
			} else {
				echo '';
			}

		} elseif ( $coupon_type == 'Deal' ) {
			if ( $show_expiration == 'Show' ) {
				if ( !empty( $expire_date ) ) {
					if ( strtotime( $expire_date ) >= strtotime( $today ) ) { ?>
                        <div class="wpcd-coupon-one-expire">
	                        <?php
	                        if ( !empty( $expire_text ) ) {
		                        echo $expire_text . ' ' . '<span class="expiration-date">' . $expire_date . '</span>'; ;
	                        } else {
		                        echo __('Expires on: ', 'wpcd-coupon') . '<span class="expiration-date">' . $expire_date . '</span>';
	                        }
	                        ?>
                        </div>
					<?php } elseif ( strtotime( $expire_date ) < strtotime( $today ) ) { ?>
                        <div class="wpcd-coupon-one-expired">
	                        <?php
	                        if ( !empty( $expired_text ) ) {
		                        echo $expired_text . ' ' . '<span class="expiration-date">' . $expire_date . '</span>'; ;
	                        } else {
		                        echo __('Expired on: ', 'wpcd-coupon') . '<span class="expiration-date">' . $expire_date . '</span>';
	                        }
	                        ?>
                        </div>
					<?php }

				} else { ?>

                    <div class="wpcd-coupon-one-expire">

						<?php	if ( !empty( $no_expiry ) ) {
							echo $no_expiry;
						} else {
							echo __("Doesn't expire", 'wpcd-coupon');
						}
						?>
                    </div>

				<?php }
			} else {
				echo '';
			}
		} ?>
        <div id="clear"></div>
    </div>
    <div id="clear"></div>
</div>

<div class="wpcd-coupon-two">
    <div class="wpcd-col-two-1-4">
		<?php if ( has_post_thumbnail() ) { ?>
            <figure>
                <img class="wpcd-coupon-two-img wpcd-get-fetured-img" src="<?php echo $coupon_thumbnail; ?>">
            </figure>
		<?php } else { ?>
            <figure>
                <img class="wpcd-coupon-two-img wpcd-get-fetured-img" src="<?php echo WPCD_Plugin::instance()->plugin_assets . 'img/icon-128x128.png'; ?>">
            </figure>
		<?php } ?>
        <div class="wpcd-coupon-two-discount-text">
	        <?php if ( !empty( $discount_text ) ) { echo $discount_text; } else { echo __('Discount Text', 'wpcd-coupon'); } ?>
        </div>
    </div>
    <div class="wpcd-col-two-3-4">
        <div class="wpcd-coupon-two-header">
            <div>
                <h4><?php if ( !empty( $title ) ) { echo $title; } else { echo __('Sample Coupon Code', 'wpcd-coupon'); } ?></h4>
            </div>
        </div>
        <div class="wpcd-coupon-two-info">
			<div class="wpcd-coupon-two-title">
                    <span><?php
	                    if ( !empty( $expire_text ) ) {
		                    echo $expire_text;
	                    } else {
		                    echo __('Expires on: ', 'wpcd-coupon');
	                    }
	                    ?>
                    </span>
                    <span class="wpcd-coupon-two-countdown" id="clock_<?php echo $post_id; ?>"></span>
                    <?php if( !$expire_date ) { $expire_date = date( 'd/m/Y' ); $expire_date_format = date( 'd/m/Y' ); } ?>
                    <script type="text/javascript">
                       var $clock = jQuery('#clock_<?php echo $post_id; ?>').countdown('<?php echo $expire_date_format . ' ' . $expire_time; ?>', function(event) {
                            var format = '%M <?php echo __( 'minutes', 'wpcd-coupon' ); ?> %S <?php echo __('seconds','wpcd-coupon'); ?>';
                            if(event.offset.hours > 0) {
                                format = "%H <?php echo __( 'hours', 'wpcd-coupon' ); ?> %M <?php echo __( 'minutes', 'wpcd-coupon' ); ?> %S <?php echo __('seconds','wpcd-coupon'); ?>";
                            }
                            if(event.offset.totalDays > 0) {
                                format = "%-d <?php echo __( 'day', 'wpcd-coupon' ); ?>%!d " + format;
                            }
                            if(event.offset.weeks > 0) {
                                format = "%-w <?php echo __( 'week', 'wpcd-coupon' ); ?>%!w " + format;
                            }
                            jQuery(this).html(event.strftime(format));

                            if(event.offset.weeks == 0 && event.offset.totalDays == 0 && event.offset.hours == 0 && event.offset.minutes == 0 && event.offset.seconds == 0){
                                jQuery(this).addClass('wpcd-countdown-expired').html('<?php echo __('This offer has expired!', 'wpcd-coupon'); ?>');
                            } else {
                                jQuery(this).html(event.strftime(format));
                                jQuery('#clock_<?php echo $post_id; ?>').removeClass('wpcd-countdown-expired');
                            }
                        });

                        jQuery("#expire-time").change(function(){
                            var coup_date = jQuery("#expire-date").val();
                            if (coup_date.indexOf("-") >= 0){
                                var dateAr = coup_date.split('-');
                                coup_date = dateAr[1] + '/' + dateAr[0] + '/' + dateAr[2];
                            }
                            selectedDate = coup_date + ' ' + jQuery("#expire-time").val();
                            $clock.countdown(selectedDate.toString());
                        });
                    </script>
            </div>
            <div class="wpcd-coupon-two-coupon">
                <div class="coupon-code-wpcd coupon-detail wpcd-coupon-button-type wpcd-coupon-hidden">
                    <a data-type="code" data-coupon-id="<?php echo $post_id; ?>" href="" class="coupon-button coupon-code-wpcd masterTooltip" id="coupon-button-<?php echo $post_id; ?>" title="<?php if (!empty($hidden_coupon_hover_text)) { echo $hidden_coupon_hover_text; } else { _e('Click Here to Show Code', 'wpcd-coupon'); } ?>" data-position="top center" data-inverted="" data-aff-url="<?php echo $link; ?>" >
                        <span class="code-text-wpcd" rel="nofollow"><?php if ( !empty( $coupon_code ) ) { echo $coupon_code; } else { echo __('COUPONCODE', 'wpcd-coupon'); } ?></span>
                        <span class="get-code-wpcd">
                            <?php
	                        if ( !empty( $hide_coupon_text ) ) {
		                        echo $hide_coupon_text;
	                        }  else {
		                        echo __('Show Code', 'wpcd-coupon');
	                        }
	                        ?>
                        </span>
                    </a>
                </div>
                <div class="wpcd-coupon-not-hidden">
                    <div class="wpcd-coupon-code">
                        <button class="wpcd-btn masterTooltip wpcd-coupon-button" title="<?php echo __('Click Here To Copy Coupon', 'wpcd-coupon'); ?>" data-clipboard-text="<?php if ( !empty( $coupon_code ) ) { echo $coupon_code; } else { echo __('COUPONCODE', 'wpcd-coupon'); } ?>">
                            <span class="wpcd_coupon_icon"></span> <span class="coupon-code-button"><?php if ( !empty( $coupon_code ) ) { echo $coupon_code; } else { echo __('COUPONCODE', 'wpcd-coupon'); } ?></span>
                        </button>
                    </div>
                    <div class="wpcd-deal-code">
                        <button class="wpcd-btn masterTooltip wpcd-deal-button" title="<?php echo __( 'Click Here To Get this deal', 'wpcd-coupon'); ?>" data-clipboard-text="<?php if ( !empty( $deal_text ) ) { echo $deal_text; } else { echo __('Claim This Deal', 'wpcd-coupon'); } ?>">
                            <span class="wpcd_deal_icon"></span><span class="deal-code-button"><?php if ( !empty( $deal_text ) ) { echo $deal_text; } else { echo __( 'Claim This Deal', 'wpcd-coupon'); } ?></span>
                        </button>
                    </div>
                </div>
            </div>
            <div id="clear"></div>
        </div>
        <div id="clear"></div>
        <div class="wpcd-coupon-description">
	        <?php if ( !empty( $description ) ) { echo $description; } else { echo __('This is the description of the coupon code. You can add additional details about the coupon here, what the coupon or deal is.', 'wpcd-coupon'); } ?>
        </div>
    </div>
</div>

<div class="wpcd-coupon-three">
    <div class="wpcd-coupon-three-content">
        <h4 class="wpcd-coupon-three-title"><?php if ( !empty( $title ) ) { echo $title; } else { echo __('Sample Coupon Code', 'wpcd-coupon'); } ?></h4>
        <div class="wpcd-coupon-description">
	        <?php if ( !empty( $description ) ) { echo $description; } else { echo __('This is the description of the coupon code. You can add additional details about the coupon here, what the coupon or deal is.', 'wpcd-coupon'); } ?>
        </div>
    </div>
    <div class="wpcd-coupon-three-info">
        <div class="wpcd-coupon-three-info-left">
            <div class="coupon-expiration wpcd-coupon-three-expire">
                <p class="wpcd-coupon-three-expire-text">
                    <span>
                        <?php
                        if ( !empty( $expire_text ) ) {
	                        echo $expire_text;
                        } else {
	                        echo __('Expires on: ', 'wpcd-coupon');
                        }
                        ?>
                    </span>
                    <span class="expiration-date"></span></p>
            </div>
		<?php
		if ( $coupon_type == 'Coupon' ) {
			if ( $show_expiration == 'Show' ) {
				if ( !empty( $expire_date ) ) {
					if ( strtotime( $expire_date ) >= strtotime( $today ) ) { ?>
                        <div class="wpcd-coupon-three-expire">
                            <p class="wpcd-coupon-three-expire-text"><?php
	                            if ( !empty( $expire_text ) ) {
		                            echo $expire_text . ' ' . '<span class="expiration-date">' . $expire_date . '</span>'; ;
	                            } else {
		                            echo __('Expires on: ', 'wpcd-coupon') . '<span class="expiration-date">' . $expire_date . '</span>';
	                            }
	                            ?></p><p>  </p>
                        </div>
					<?php } elseif ( strtotime( $expire_date ) < strtotime( $today ) ) { ?>
                        <div class="wpcd-coupon-three-expire">
                            <p class="wpcd-coupon-three-expired">
	                            <?php
	                            if ( !empty( $expired_text ) ) {
		                            echo $expired_text . ' ' . '<span class="expiration-date">' . $expire_date . '</span>'; ;
	                            } else {
		                            echo __('Expired on: ', 'wpcd-coupon') . '<span class="expiration-date">' . $expire_date . '</span>';
	                            }
	                            ?>
                            </p>
                        </div>
					<?php }
				} else { ?>
                    <div class="wpcd-coupon-three-expire">
						<?php	if ( !empty( $no_expiry ) ) { ?>
                            <p><?php echo $no_expiry; ?></p>
						<?php } else { ?>
                            <p><?php echo __("Doesn't expire", 'wpcd-coupon'); ?></p>
						<?php }
						?>
                    </div>
				<?php }
			} else {
				echo '';
			}

		} elseif ( $coupon_type == 'Deal' ) {
			if ( $show_expiration == 'Show' ) {
				if ( !empty( $expire_date ) ) {
					if ( strtotime( $expire_date ) >= strtotime( $today ) ) { ?>
                        <div class="wpcd-coupon-three-expire">
                            <p>
                                <?php
	                            if ( !empty( $expire_text ) ) {
		                            echo $expire_text . ' ' . '<span class="expiration-date">' . $expire_date . '</span>'; ;
	                            } else {
		                            echo __('Expires on: ', 'wpcd-coupon') . '<span class="expiration-date">' . $expire_date . '</span>';
	                            }
	                            ?>
                            </p>
                        </div>
					<?php } elseif ( strtotime( $expire_date ) < strtotime( $today ) ) { ?>
                        <div class="wpcd-coupon-three-expire">
                            <p class="wpcd-coupon-three-expired">
	                            <?php
	                            if ( !empty( $expired_text ) ) {
		                            echo $expired_text . ' ' . '<span class="expiration-date">' . $expire_date . '</span>'; ;
	                            } else {
		                            echo __('Expired on: ', 'wpcd-coupon') . '<span class="expiration-date">' . $expire_date . '</span>';
	                            }
	                            ?>
                            </p>
                        </div>
					<?php }

				} else { ?>

                    <div class="wpcd-coupon-three-expire">

						<?php	if ( !empty( $no_expiry ) ) { ?>
                            <p><?php echo $no_expiry; ?></p>
						<?php } else ?>
                            <p><?php echo __("Doesn't expire", 'wpcd-coupon'); ?></p>
				        <?php }
						?>
                    </div>

				<?php }
			} else {
				echo '';
			}
		?>
        </div>
        <div class="wpcd-coupon-three-coupon">
            <div class="coupon-code-wpcd coupon-detail wpcd-coupon-button-type wpcd-coupon-hidden">
                <a data-type="code" data-coupon-id="<?php echo $post_id; ?>" href="" class="coupon-button coupon-code-wpcd masterTooltip" id="coupon-button-<?php echo $post_id; ?>" title="<?php if (!empty($hidden_coupon_hover_text)) { echo $hidden_coupon_hover_text; } else { _e('Click Here to Show Code', 'wpcd-coupon'); } ?>" data-position="top center" data-inverted="" data-aff-url="<?php echo $link; ?>" >
                    <span class="code-text-wpcd" rel="nofollow"><?php if ( !empty( $coupon_code ) ) { echo $coupon_code; } else { echo __('COUPONCODE', 'wpcd-coupon'); } ?></span>
                    <span class="get-code-wpcd">
                        <?php
                        if ( !empty( $hide_coupon_text ) ) {
	                        echo $hide_coupon_text;
                        }  else {
	                        echo __('Show Code', 'wpcd-coupon');
                        }
                        ?>
                    </span>
                </a>
            </div>
            <div class="wpcd-coupon-not-hidden">
                <div class="wpcd-coupon-code">
                    <button class="wpcd-btn masterTooltip wpcd-coupon-button" title="<?php echo __('Click Here To Copy Coupon', 'wpcd-coupon'); ?>" data-clipboard-text="<?php if ( !empty( $coupon_code ) ) { echo $coupon_code; } else { echo __('COUPONCODE', 'wpcd-coupon' ); } ?>">
                        <span class="wpcd_coupon_icon"></span> <span class="coupon-code-button"><?php if ( !empty( $coupon_code ) ) { echo $coupon_code; } else { echo __('COUPONCODE', 'wpcd-coupon'); } ?></span>
                    </button>
                </div>
                <div class="wpcd-deal-code">
                    <button class="wpcd-btn masterTooltip wpcd-deal-button" title="<?php echo __('Click Here To Get this deal', 'wpcd-coupon'); ?>" data-clipboard-text="<?php if ( !empty( $deal_text ) ) { echo $deal_text; } else { echo __('Claim This Deal', 'wpcd-coupon'); } ?>">
                        <span class="wpcd_deal_icon"></span><span class="deal-code-button"><?php if ( !empty( $deal_text ) ) { echo $deal_text; } else { echo __('Claim This Deal', 'wpcd-coupon'); } ?></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<p><i><strong><?php echo __('Note:', 'wpcd-coupon'); ?></strong> <?php echo __('This is just to show how the coupon will look. Click to copy functionality, showing hidden coupon will not work here, but it will work on posts, pages where you put the shortcode.', 'wpcd-coupon'); ?></i></p>

