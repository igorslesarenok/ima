<?php	
	global $pmpro_affiliates_settings;
	$pmpro_affiliates_singular_name = $pmpro_affiliates_settings['pmpro_affiliates_singular_name'];
	$pmpro_affiliates_plural_name = $pmpro_affiliates_settings['pmpro_affiliates_plural_name'];
	$aff_discount_member_reg = $pmpro_affiliates_settings['aff_discount_member_reg'];
	$aff_commission_member_reg = $pmpro_affiliates_settings['aff_commission_member_reg'];
	$aff_discount_affiliate_reg = $pmpro_affiliates_settings['aff_discount_affiliate_reg'];
	$aff_commission_affiliate_reg = $pmpro_affiliates_settings['aff_commission_affiliate_reg'];

	if(isset($_REQUEST['report']))	
		$report = $_REQUEST['report'];
	else
		$report = false;
	
	if($report && $report != "all")
	{
		//get values from DB
		$affiliate_id = $report;		
		$affiliate = $wpdb->get_row("SELECT * FROM $wpdb->pmpro_affiliates WHERE id = '" . intval($affiliate_id) . "' LIMIT 1");
		if(!empty($affiliate) && !empty($affiliate->id))
		{
			$code = $affiliate->code;
			$name = $affiliate->name;
			$affiliateuser = $affiliate->affiliateuser;
			$trackingcode = $affiliate->trackingcode;
			$cookiedays = $affiliate->cookiedays;
			$enabled = $affiliate->enabled;
		}
	}	
?>
	<h2>
		<?php echo ucwords($pmpro_affiliates_singular_name); ?> Report
		<?php 
			if(empty($affiliate_id))
				echo "for All " . ucwords($pmpro_affiliates_plural_name);
			else
				echo "for Code " . stripslashes($code);
		?>
		
		<?php 
			if(!empty($affiliate_id))
			{
				?>
				<a href="admin.php?page=pmpro-affiliates&report=all" class="add-new-h2">View All <?php echo ucwords($pmpro_affiliates_plural_name); ?> Report</a>
				<?php
			}
		?>
	</h2>
<?php
	if(!empty($name))
		echo "<p>Business/Contact Name: " . stripslashes($name) . "</p>";
	if(!empty($affiliateuser))
		echo "<p>" . ucwords($pmpro_affiliates_singular_name) . " User: " . stripslashes($affiliateuser) . "</p>";
?>

<?php

if(!empty($affiliate_id) && $report != "all" ){
	$last_payout_date = $wpdb->get_var("SELECT last_payout_date FROM $wpdb->pmpro_affiliates WHERE id = '" . esc_sql($affiliate_id) . "' ");
}

if(!empty($last_payout_date)){
?>

<h3>Payouts log</h3>
<table class="widefat">
<thead>
	<tr>				
		<th><?php _e('Code', 'pmpro_affiliates'); ?></th>
		<th><?php _e('Affiliates name', 'pmpro_affiliates'); ?></th>
		<th><?php _e('Date', 'pmpro_affiliates'); ?></th>
		<th><?php _e('Total', 'pmpro_affiliates'); ?></th>
		<th><?php _e('Status', 'pmpro_affiliates'); ?></th>
	</tr>
</thead>
<tbody>
<?php

	$count = 0;
	$sqlQuery = "SELECT a.code, a.name, UNIX_TIMESTAMP(o.date) as date, o.money FROM $wpdb->pmpro_affiliates_payouts o LEFT JOIN $wpdb->pmpro_affiliates a ON o.aid = a.id WHERE a.id = ".$affiliate_id." ";
	/*
	if($report != "all")
		$sqlQuery .= " AND a.id = '" . esc_sql($report) . "' ";
	*/
	$affiliate_orders = $wpdb->get_results($sqlQuery);	
	if(empty($affiliate_orders))
	{
	?>
		<tr><td colspan="6" class="pmpro_pad20">					
			<p><?php echo "There was no payouts yet."; ?></p>
		</td></tr>
	<?php
	}
	else
	{
		global $pmpro_currency_symbol;
		foreach($affiliate_orders as $order)
		{ 	
		?>
		<tr<?php if($count++ % 2 == 1) { ?> class="alternate"<?php } ?>>
			<td><?php echo $order->code;?></td>
			<td><?php echo stripslashes($order->name);?></td>
			<td><?php echo date_i18n("F j, Y g:i a", $order->date);?></td>
			<td><?php echo $pmpro_currency_symbol . $order->money;?></td>
			<td><span class="rewards_status" >OK</span></td>
		</tr>
		<?php
		}
	}
?>
</tbody>
</table>
<p></p>
<p></p>


<h3>All orders</h3>
<?php } ?>
<a href="<?php echo admin_url('admin-ajax.php');?>?action=affiliates_report_csv&report=<?php echo $report;?>" class="add-new-h2">Export to CSV</a>
<table class="widefat">
<thead>
	<tr>				
		<th><?php _e('Code', 'pmpro_affiliates'); ?></th>
		<!--<th><?php _e('Sub-ID', 'pmpro_affiliates'); ?></th>-->
		<th><?php _e('Name', 'pmpro_affiliates'); ?></th>
		<th><?php _e('Member', 'pmpro_affiliates'); ?></th>
		<th><?php _e('Date', 'pmpro_affiliates'); ?></th>
		<th><?php _e('Commission', 'pmpro_affiliates'); ?></th>
	</tr>
</thead>
<tbody>
	<?php

		$count = 0;
		$sqlQuery = "SELECT a.code, o.membership_id, o.affiliate_subid as subid, a.name, u.user_login, UNIX_TIMESTAMP(o.timestamp) as timestamp, o.total FROM $wpdb->pmpro_membership_orders o LEFT JOIN $wpdb->pmpro_affiliates a ON o.affiliate_id = a.id LEFT JOIN $wpdb->users u ON o.user_id = u.ID WHERE o.affiliate_id <> '' ";
		if($report != "all")
			$sqlQuery .= " AND a.id = '" . esc_sql($report) . "' ";
		$affiliate_orders = $wpdb->get_results($sqlQuery);
		if(empty($affiliate_orders))
		{
		?>
			<tr><td colspan="6" class="pmpro_pad20">					
				<p><?php echo sprintf('No %s signups have been tracked yet.', $pmpro_affiliates_singular_name, 'pmpro_affiliates'); ?></p>
			</td></tr>
		<?php
		}
		else
		{
			global $pmpro_currency_symbol;
			foreach($affiliate_orders as $order)
			{ 	
				$uid = get_user_by('login',$order->user_login)->ID;
			?>
			<tr<?php if($count++ % 2 == 1) { ?> class="alternate"<?php } ?>>
				<td><?php echo $order->code;?></td>
				<!--<td><?php echo $order->subid;?></td>-->
				<td><?php echo stripslashes($order->name);?></td>
				<td><?php echo '<a href="'.get_admin_url(NULL, "user-edit.php?user_id=".$uid).'" >'.$order->user_login.'</a>';?></td>
				<td><?php echo date_i18n("F j, Y g:i a", $order->timestamp);?></td>
				<td>
					<?php 
					
					
						$levelDetails = $wpdb->get_row("SELECT * FROM $wpdb->pmpro_membership_levels WHERE id = '" . esc_sql( $order->membership_id) . "' ");
								
						if(count($levelDetails)){
							if( $levelDetails->cycle_period == "Month" && $levelDetails->cycle_number > 0 ){
								if( in_array($order->membership_id, $aff_commission_member_reg['levels']) ){
									$earnings = ($levelDetails->cycle_number*$aff_commission_member_reg['value']);
								}else if( in_array($order->membership_id, $aff_commission_affiliate_reg['levels']) ){
									$earnings = ($levelDetails->cycle_number*$aff_commission_affiliate_reg['value']);
								}
								
							}
						}		
					
						echo $pmpro_currency_symbol . $earnings;
											
					?>
				</td>
			</tr>
			<?php
			}
		}
	?>
</tbody>
</table>