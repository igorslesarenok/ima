<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Hooks to the different forms and adds extra fields, required by the checkout process,
 * at the bottom of each form
 *
 */
function pms_add_checkout_extra_fields() {

    /**
     * Dynamic hook to set extra checkout field sections
     *
     */
    $checkout_sections = apply_filters( 'pms_checkout_sections', array() );

    if( empty( $checkout_sections ) )
        return;

    /**
     * Dynamic hook to set extra checkout fields
     *
     */
    $checkout_fields = apply_filters( 'pms_checkout_fields', array() );

    if( empty( $checkout_fields ) )
        return;

    // Go through each section and output the attached fields
    foreach( $checkout_sections as $section ) {

        if( empty( $section['name'] ) )
            continue;

        // Set section element tag type
        $section_element = ( ! empty( $section['element'] ) ? $section['element'] : 'div' );

        // Opening element tag of the section
        echo '<' . esc_attr( $section_element ) . ' ' . ( ! empty( $section['id'] ) ? 'id="' . esc_attr( $section['id'] ) . '"' : '' ) . ' class="pms-field-section ' . ( ! empty( $section['class'] ) ? esc_attr( $section['class'] ) : '' ) . '">';

        // Output each field
        foreach( $checkout_fields as $field ) {

            if( $field['section'] == $section['name'] )
                pms_output_form_field( $field );

        }

        // Closing element tag of each section
        echo '</' . esc_attr( $section_element ) . '>';

    }

}
add_action( 'pms_register_form_bottom', 'pms_add_checkout_extra_fields', 50 );
add_action( 'pms_new_subscription_form_bottom', 'pms_add_checkout_extra_fields', 50 );
add_action( 'pms_upgrade_subscription_form_bottom', 'pms_add_checkout_extra_fields', 50 );
add_action( 'pms_renew_subscription_form_bottom', 'pms_add_checkout_extra_fields', 50 );
add_action( 'pms_retry_payment_form_bottom', 'pms_add_checkout_extra_fields', 50 );

/**
 * Hooks to the Profile Builder subscription plans field to add the extra checkout fields
 *
 * @param string $output
 *
 * @return string
 *
 */
function pms_pb_add_checkout_extra_fields( $output = '' ) {

	ob_start();

	// Call the extra checkout fields adder
	pms_add_checkout_extra_fields();

	$extra_fields_output = ob_get_contents();
	ob_end_clean();

	return $output . $extra_fields_output;

}
add_filter( 'wppb_register_subscription_plans_field', 'pms_pb_add_checkout_extra_fields', 50 );


/**
 * Returns the output of a checkout form field, given a set of parameters for the field
 *
 * @param array $field
 *
 * @return string
 *
 */
function pms_output_form_field( $field = array() ) {

    if( empty( $field['type'] ) )
        return;

    
    /**
     * If the field has custom content output it
     *
     */
    if( has_action( 'pms_output_form_field_' . $field['type'] ) ) {

    	/**
	     * Action hook to dynamically add custom content for the field
	     * This way one can overwrite the default output of a field
	     *
	     * @param string $field_inner_output
	     * @param array  $field
	     *
	     */
    	do_action( 'pms_output_form_field_' . $field['type'], $field );

    /**
     * If the field does not have custom content output the default field HTML
     *
     */
    } else {

    	// Determine field wrapper element tag
	    $field_element_wrapper = ( ! empty( $field['element_wrapper'] ) ? $field['element_wrapper'] : 'div' );

	    // Opening element tag of the field
	    echo '<' . esc_attr( $field_element_wrapper ) . ' class="pms-field pms-field-' . esc_attr( $field['type'] ) . ' ' . ( ! empty( $field['wrapper_class'] ) ? esc_attr( $field['wrapper_class'] ) : '' ) . '">';

	    // Field label
	    if( ! empty( $field['label'] ) )
	        echo '<label ' . ( ! empty( $field['name'] ) ? 'for="' . esc_attr( $field['name'] ) . '"' : '' ) . '>' . esc_attr( $field['label'] ) . '</label>';

	    echo '<div class="pms-field-input-container">';

	    /**
	     * Action hook to dynamically add the actual input HTML for the field
	     *
	     * @param array $field
	     *
	     */
	    do_action( 'pms_output_form_field_inner_' . $field['type'], $field );

	    echo '</div>';

	    // Field description
	    if( ! empty( $field['description'] ) )
	        echo '<p class="pms-field-description">' . esc_attr( $field['description'] ) . '</p>';

	    // Field errors
	    if( ! empty( $field['name'] ) ) {

	    	$errors = pms_errors()->get_error_messages( $field['name'] );
	    	
	    	if( ! empty( $errors ) )
	    		pms_display_field_errors( $errors );

	    }

	    // Closing element tag of each section
	    echo '</' . esc_attr( $field_element_wrapper ) . '>';

    }

}


/**
 * Outputs the "heading" type form field
 *
 * @param array $field
 *
 */
function pms_output_form_field_heading( $field = array() ) {

	if( $field['type'] != 'heading' )
		return;

	if( empty( $field['default'] ) )
		return;

	// Determine field wrapper element tag
    $field_element_wrapper = ( ! empty( $field['element_wrapper'] ) ? $field['element_wrapper'] : 'div' );

    // Opening element tag of the field
    $output  = '<' . esc_attr( $field_element_wrapper ) . ' class="pms-field pms-field-' . esc_attr( $field['type'] ) . ' ' . ( ! empty( $field['wrapper_class'] ) ? esc_attr( $field['wrapper_class'] ) : '' ) . '">';

    $output .= wp_kses_post( $field['default'] );

    // Closing element tag of each section
    $output .= '</' . esc_attr( $field_element_wrapper ) . '>';

    echo $output;

}
add_action( 'pms_output_form_field_heading', 'pms_output_form_field_heading' );


/**
 * Outputs the "checkbox_single" type form field
 *
 * @param array $field
 *
 */
function pms_output_form_field_checkbox_single( $field = array() ) {

	if( $field['type'] != 'checkbox_single' )
		return;

	if( empty( $field['name'] ) )
		return;

	// Determine field wrapper element tag
    $field_element_wrapper = ( ! empty( $field['element_wrapper'] ) ? $field['element_wrapper'] : 'div' );

    // Opening element tag of the field
    $output  = '<' . esc_attr( $field_element_wrapper ) . ' class="pms-field pms-field-' . esc_attr( $field['type'] ) . ' ' . ( ! empty( $field['wrapper_class'] ) ? esc_attr( $field['wrapper_class'] ) : '' ) . '">';

    // Set value
	$value = ( isset( $field['value'] ) ? $field['value'] : ( isset( $field['default'] ) ? $field['default'] : '' ) );

    $output .= '<input type="checkbox" id="' . esc_attr( $field['name'] ) . '" name="' . esc_attr( $field['name'] ) . '" value="1" ' . ( ! empty( $value ) ? 'checked' : '' ) . ' />';
    $output .= '<label for="' . esc_attr( $field['name'] ) . '">' . ( ! empty( $field['label'] ) ? $field['label'] : '' ) . '</label>';

    // Field description
    if( ! empty( $field['description'] ) )
        $output .= '<p class="pms-field-description">' . esc_attr( $field['description'] ) . '</p>';

    // Output errors
    $errors = pms_errors()->get_error_messages( $field['name'] );
	    	
	if( ! empty( $errors ) )
		pms_display_field_errors( $errors );

    // Closing element tag of each section
    $output .= '</' . esc_attr( $field_element_wrapper ) . '>';

    echo $output;

}
add_action( 'pms_output_form_field_checkbox_single', 'pms_output_form_field_checkbox_single' );


/**
 * Outputs the inner field content of the "text" type form field
 *
 * @param array $field
 *
 */
function pms_output_form_field_inner_text( $field = array() ) {

	if( $field['type'] != 'text' )
		return;

	if( empty( $field['name'] ) )
		return;

	// Set value
	$value = ( isset( $field['value'] ) ? $field['value'] : ( isset( $field['default'] ) ? $field['default'] : '' ) );

	// Field output
	$output = '<input type="text" id="' . esc_attr( $field['name'] ) . '" name="' . esc_attr( $field['name'] ) . '" value="' . esc_attr( $value ) . '" />';

	echo $output;

}
add_action( 'pms_output_form_field_inner_text', 'pms_output_form_field_inner_text', 10, 2 );


/**
 * Outputs the inner field content of the "select" type form field
 *
 * @param array $field
 *
 */
function pms_output_form_field_inner_select( $field = array() ) {

	if( $field['type'] != 'select' )
		return;

	if( empty( $field['name'] ) )
		return;

	// Set value
	$value = ( isset( $field['value'] ) ? $field['value'] : ( isset( $field['default'] ) ? $field['default'] : '' ) );

	// Field output
	$output  = '<select id="' . esc_attr( $field['name'] ) . '" name="' . esc_attr( $field['name'] ) . '">';

		if( ! empty( $field['options'] ) ) {

			foreach( $field['options'] as $option_value => $option_label )
				$output .= '<option value="' . esc_attr( $option_value ) . '" ' . ( $value == $option_value ? 'selected' : '' ) . '>' . esc_attr( $option_label ) . '</option>';

		}

	$output .= '</select>';

	echo $output;

}
add_action( 'pms_output_form_field_inner_select', 'pms_output_form_field_inner_select', 10, 2 );


/**
 * Outputs the inner field content of the "checkbox" type form field
 *
 * @param array $field
 *
 */
function pms_output_form_field_inner_checkbox( $field = array() ) {

	if( $field['type'] != 'checkbox' )
		return;

	if( empty( $field['name'] ) )
		return;

	// Set values
	$values = ( isset( $field['value'] ) ? $field['value'] : ( isset( $field['default'] ) ? $field['default'] : array() ) );
	$output = '';

	// Output each checkbox
	if( ! empty( $field['options'] ) ) {

		foreach( $field['options'] as $option_value => $option_label ) {

			$output .= '<label>';
			$output .= '<input type="checkbox" name="' . esc_attr( $field['name'] ) . '[]" value="' . esc_attr( $option_value ) . '" ' . ( in_array( $option_value, $values ) ? 'checked' : '' ) . ' />';
			$output .= esc_attr( $option_label );
			$output .= '</label>';

		}

	}

	echo $output;

}
add_action( 'pms_output_form_field_inner_checkbox', 'pms_output_form_field_inner_checkbox', 10, 2 );


/**
 * Outputs the inner field content of the "radio" type form field
 *
 * @param array $field
 *
 */
function pms_output_form_field_inner_radio( $field = array() ) {

	if( $field['type'] != 'radio' )
		return;

	if( empty( $field['name'] ) )
		return;

	// Set value
	$value  = ( isset( $field['value'] ) ? $field['value'] : ( isset( $field['default'] ) ? $field['default'] : '' ) );
	$output = '';

	// Output each radio
	if( ! empty( $field['options'] ) ) {

		foreach( $field['options'] as $option_value => $option_label ) {

			$output .= '<label>';
			$output .= '<input type="radio" name="' . esc_attr( $field['name'] ) . '" value="' . esc_attr( $option_value ) . '" ' . ( $value == $option_value ? 'checked' : '' ) . ' />';
			$output .= esc_attr( $option_label );
			$output .= '</label>';

		}

	}

	echo $output;

}
add_action( 'pms_output_form_field_inner_radio', 'pms_output_form_field_inner_radio', 10, 2 );


/*
 * Example on how to register a checkout section
 *
 * Please do not uncomment this function as it will add sections to your checkout
 *
 *
function pms_example_register_checkout_sections( $sections ) {

	$sections[] = array(
		'name'    => 'example_section',
		'element' => 'ul',
		'class'	  => 'extra_class'
	);

	return $sections;

}
add_filter( 'pms_checkout_sections', 'pms_example_register_checkout_sections' );

/*
 * Example of how to register different form fields
 *
 * Please do not uncomment this function as it will add fields to your checkout
 *
 *
function pms_example_register_checkout_fields( $fields ) {

	// Adding a field type "heading"
	$fields[] = array(
		'section' 		  => 'example_section',
		'type' 			  => 'heading',
		'default' 		  => '<h3>Section heading</h3>',
		'element_wrapper' => 'li'
	);

	// Adding a field type "text"
	$fields[] = array(
		'section' 		  => 'example_section',
		'type' 			  => 'text',
		'name' 			  => 'my_first_text_field',
		'default' 		  => 'This is the default text',
		'value' 		  => 'This is the value of the field, which will overwrite the default text',
		'label' 		  => 'My text field label',
		'description' 	  => 'Description for text field',
		'element_wrapper' => 'li'
	);

	// Adding a field type "checkbox_single"
	$fields[] = array(
		'section' 		  => 'example_section',
		'type' 			  => 'checkbox_single',
		'name' 			  => 'my_first_checkbox_single_field',
		'label' 		  => 'My first checkbox single label',
		'description' 	  => 'Description for checkbox single field',
		'element_wrapper' => 'li'
	);

	// Adding a field type "checkbox"
	$fields[] = array(
		'section' 		  => 'example_section',
		'type' 			  => 'checkbox',
		'name' 			  => 'my_first_checkbox_field',
		'default' 		  => array( 'option_1' ),
		'label' 		  => 'My checkbox field label',
		'options' 		  => array(
			'option_1' => 'Option 1',
			'option_2' => 'Option 2',
			'option_3' => 'Option 3'
		),
		'description' 	  => 'Description for checkbox field',
		'element_wrapper' => 'li'
	);

	// Adding a field type "select"
	$fields[] = array(
		'section' 		  => 'example_section',
		'type' 			  => 'select',
		'name' 			  => 'my_first_select_field',
		'default' 		  => 'option_2',
		'label' 		  => 'My select field label',
		'options' 		  => array(
			'option_1' => 'Option 1',
			'option_2' => 'Option 2',
			'option_3' => 'Option 3'
		),
		'description' 	  => 'Description for select field',
		'element_wrapper' => 'li'
	);

	// Adding a field type "radio"
	$fields[] = array(
		'section' 		  => 'example_section',
		'type' 			  => 'radio',
		'name' 			  => 'my_first_radio_field',
		'default' 		  => 'option_1',
		'label' 		  => 'My radio field label',
		'options' 		  => array(
			'option_1' => 'Option 1',
			'option_2' => 'Option 2',
			'option_3' => 'Option 3'
		),
		'description' 	  => 'Description for radio field',
		'element_wrapper' => 'li'
	);

	return $fields;

}
add_filter( 'pms_checkout_fields', 'pms_example_register_checkout_fields' );
*/