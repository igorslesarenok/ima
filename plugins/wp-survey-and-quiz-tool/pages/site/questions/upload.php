			<div class="wpsqt_multiple_question upload">
			<?php
				if (isset($question['randomize_answers']) && $question['randomize_answers'] == 'yes') {
					$answers = array();
					while (count($question['answers']) > 0) {
						$key = array_rand($question['answers']);
						$answers[$key] = $question['answers'][$key];
						unset($question['answers'][$key]);
					}
					$question['answers'] = $answers;

					// Store the order of the answers for review page
					$_SESSION['wpsqt'][$quizName]['sections'][$sectionKey]['questions'][$questionKey]['answers'] = $answers;
				}
			?>
			<?php foreach ( $question['answers'] as $answerKey => $answer ){ ?>
				<div>
					<p></p>
					<?php

						echo do_shortcode('[ajax-file-upload unique_identifier="upload_ans_mp_w"   on_success_set_input_value="#upload_ans" allowed_extensions="pdf,txt,doc,docx,rtf" ]');  
						
					?>
					<p></p>
					<div class="kni_success_box" style="display: none; " >File uploaded successfully!</div>
					<div class="kni_error_box" style="display: none; " >Error.</div>
					<p></p>
					<script>
						window.addEventListener( "afu_file_uploaded", function(e){
							if( "undefined" !== typeof e.data.response.media_uri ) {
								jQuery('.kni_success_box').fadeIn(); 
							}
						}, false);
						
						window.addEventListener( "afu_upload_loading", function(e){
							jQuery('.kni_success_box, .kni_error_box').fadeOut();
						}, false);
						
						window.addEventListener( "afu_error_uploading", function(e){
							jQuery('.kni_error_box').fadeOut(); 
						}, false);
						
					</script>
					<input id="upload_ans" type="hidden"name="answers[<?php echo $questionKey; ?>][]" />
					<!--<input type="file" name="answers[<?php echo $questionKey; ?>][]" value="<?php echo $answerKey; ?>" id="answer_<?php echo $question['id']; ?>_<?php echo $answerKey;?>" <?php if ( (isset($answer['default']) && $answer['default'] == 'yes') || in_array($answerKey, $givenAnswer)) {  ?> checked="checked" <?php } ?> /> <p for="answer_<?php echo $question['id']; ?>_<?php echo $answerKey;?>"></p>--> 
				</div>
			<?php } 
				if (    $question['type'] == 'Multiple Choice' 
					 && array_key_exists('include_other',$question)
					 && $question['include_other'] == 'yes' ){					
				?>
				<div>
					<input type="checkbox" name="answers[<?php echo $questionKey; ?>]" value="0" id="answer_<?php echo $question['id']; ?>_other"> <label for="answer_<?php echo $question['id']; ?>_other"><?php _e('Other', 'wp-survey-and-quiz-tool'); ?></label> <input type="text" name="other[<?php echo $questionKey; ?>]" value="" />
				</div>
				<?php } ?>
			</div>